import Controller from '@ember/controller';
import { inject as service } from '@ember/service';
import { alias } from '@ember/object/computed';
import { computed } from '@ember/object';

export default Controller.extend({
  store: service(),
  currentUser: service(),

  researches: alias('currentUser.user.allResearches'),

  init() {
    this._super(...arguments);

    let groups = this.get('currentUser.user.groups');
    if (groups.get('isPending')) groups.reload();
  },

  groups: computed('currentUser.user.groups.isPending', function() {
    return this.get('currentUser.user.groups').sortBy('name');
  }),

  actions: {
    new() {
      let user = this.get('currentUser.user');
      let research = this.get('store').createRecord('research', { user: user });
    }
  }
});
