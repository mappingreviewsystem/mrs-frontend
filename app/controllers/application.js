import Controller from '@ember/controller';
import FileSaverMixin from 'ember-cli-file-saver/mixins/file-saver';
import { inject as service } from '@ember/service';
import { computed } from '@ember/object';
import { alias } from '@ember/object/computed';

export default Controller.extend(FileSaverMixin, {
  session: service(),
  currentUser: service(),
  router: service(),
  notesSidenav: service(),

  isLoadingRoute: false,
  isConfig: false,
  routeBeforeConfig: 'planning',
  research: alias('currentUser.currentResearch.content'),
  isNotesSidenavOpen: alias('notesSidenav.isOpen'),
  notesSidenavModel: alias('notesSidenav.model'),
  routeName: computed('router.currentRouteName', function() {
    let route = this.get('router.currentRouteName');
    return route.split('.')[0];
  }),

  // TODO: remove when not necessary
  convertB64ToBinary: function(base64) {
    var raw = window.atob(base64);
    var rawLength = raw.length;
    var array = new Uint8Array(new ArrayBuffer(rawLength));

    for(let i = 0; i < rawLength; i++) {
      array[i] = raw.charCodeAt(i);
    }
    return array;
  },

  actions: {
    changeTab(value) {
      this.transitionToRoute(value);
    },
    control() {
      if (this.get('isConfig')) {
        this.set('isConfig', false);
        this.transitionToRoute(this.get('routeBeforeConfig'));
      }
      else {
        this.set('isConfig', true);
        this.set('routeBeforeConfig', this.get('router.currentRouteName'));
        this.transitionToRoute('researches');
      }
    },
    invalidateSession() {
      this.get('session').invalidate();
    },
    getSpreadsheet() {
      if (!confirm("Você deseja baixar um planilha com os dados da sua pesquisa?")) return;

      this.get('research').spreadsheet().then((content) => {
        let title = this.get('research.title');
        title = title.replace(/(\/|\\\\|\?|%|\*|:|\||"|<|>|\.| )/g, '_');
        title = title + '_' + new Date().toISOString() + '.xlsx';

        // TODO: hack because $.ajax don't support binary responses, fix when possible
        var binary = this.get('convertB64ToBinary')(content);
        this.saveFileAs(title, binary, 'text/excel');
      });
    },
    toggleSidenav(model) {
      this.get('notesSidenav').toggle(model);
    }
  }
});
