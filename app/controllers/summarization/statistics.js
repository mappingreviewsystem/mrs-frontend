import Controller from '@ember/controller';
import { inject as service } from '@ember/service';
import { alias } from '@ember/object/computed';
import { task } from 'ember-concurrency';
import { getNames } from 'ember-i18n-iso-countries';

export default Controller.extend({
  currentUser: service(),
  store: service(),

  statistics: undefined,
  research: alias('currentUser.currentResearch.content'),

  init() {
    this._super(...arguments);

    this.get('research').statistics().then((response) => {
      this.set('statistics', response.data.attributes);
      this.get('mountStatistics').perform();
    });
  },

  countries: getNames('pt'),

  mountStatistics: task(function * () {
    let statistics = this.get('statistics');
    let _this = this;

    let authorArticles = [];
    for (var authorId in statistics['author_articles']) {
      if (!statistics['author_articles'].hasOwnProperty(authorId)) continue;
      let articlesIds = statistics['author_articles'][authorId];

      let author = yield this.get('store').findRecord('author', authorId);
      let articles = articlesIds.map((articleId) => { return _this.get('store').findRecord('article', articleId) });
      authorArticles.pushObject({author, articlesCount: articles.length, articles});
    }
    this.set('authorArticles', authorArticles.sort((a, b) => { return b.articlesCount - a.articlesCount; }));

    let yearArticles = [];
    let yearArticlesChartYear = []
    let yearArticlesChartCount = []
    for (var year in statistics['year_articles']) {
      if (!statistics['year_articles'].hasOwnProperty(year)) continue;
      let articlesIds = statistics['year_articles'][year];

      let articles = articlesIds.map((articleId) => { return _this.get('store').findRecord('article', articleId) });
      yearArticles.pushObject({year, articlesCount: articles.length, articles});
      yearArticlesChartYear.pushObject(year);
      yearArticlesChartCount.pushObject(articles.length);
    }
    this.set('yearArticlesChart', { labels: yearArticlesChartYear, datasets: [{ label: 'articles', data: yearArticlesChartCount }] });
    this.set('yearArticles', yearArticles.sort((a, b) => { return b.year - a.year; }));

    let publicationArticles = [];
    for (var publicationId in statistics['publication_articles']) {
      if (!statistics['publication_articles'].hasOwnProperty(publicationId)) continue;
      let articlesIds = statistics['publication_articles'][publicationId];

      let publication = yield this.get('store').findRecord('publication', publicationId);
      let articles = articlesIds.map((articleId) => { return _this.get('store').findRecord('article', articleId) });
      publicationArticles.pushObject({publication, articlesCount: articles.length, articles});
    }
    this.set('publicationArticles', publicationArticles.sort((a, b) => { return b.articlesCount - a.articlesCount; }));

    let affiliationAuthors = [];
    for (var affiliationId in statistics['affiliation_authors']) {
      if (!statistics['affiliation_authors'].hasOwnProperty(affiliationId)) continue;
      let authorsIds = statistics['affiliation_authors'][affiliationId];

      let affiliation = yield this.get('store').findRecord('affiliation', affiliationId);
      let authors = authorsIds.map((authorId) => { return _this.get('store').findRecord('author', authorId) });
      affiliationAuthors.pushObject({affiliation, authorsCount: authors.length, authors});
    }
    this.set('affiliationAuthors', affiliationAuthors.sort((a, b) => { return b.authorsCount - a.authorsCount; }));

    let countryAffiliations = [];
    for (var countryId in statistics['country_affiliations']) {
      if (!statistics['country_affiliations'].hasOwnProperty(countryId)) continue;
      let affiliationsIds = statistics['country_affiliations'][countryId];

      let affiliations = affiliationsIds.map((affiliationId) => { return _this.get('store').findRecord('affiliation', affiliationId) });
      countryAffiliations.pushObject({countryId, affiliationsCount: affiliations.length, affiliations});
    }
    this.set('countryAffiliations', countryAffiliations.sort((a, b) => { return b.affiliationsCount - a.affiliationsCount; }));

    let articleDatabaseArticles = [], articlesCount = 0;
    for (var articleDatabaseId in statistics['article_database_articles']) {
      if (!statistics['article_database_articles'].hasOwnProperty(articleDatabaseId)) continue;
      let articlesIds = statistics['article_database_articles'][articleDatabaseId];

      if (articleDatabaseId == 0) {
        var articleDatabase = { title: "Avulsos (Sem base)" };
      }
      else {
        var articleDatabase = yield this.get('store').findRecord('article-database', articleDatabaseId);
      }
      let articles = articlesIds.map((articleId) => { return _this.get('store').findRecord('article', articleId) });
      articleDatabaseArticles.pushObject({articleDatabase, articlesCount: articles.length, articles});
      articlesCount += articles.length;
    }
    this.set('articleDatabaseArticles', articleDatabaseArticles.sort((a, b) => { return b.articlesCount - a.articlesCount; }));
    this.set('articlesCount', articlesCount);
  }),

  // modes: undefined,
  // selectedMode: undefined,
  //
  // init() {
  //   this._super(...arguments);
  //
  //   let modes = [
  //     { 'name': 'table', 'icon': 'table'},
  //     { 'name': 'chart', 'icon': 'bar-chart'},
  //     { 'name': 'map', 'icon': 'globe'},
  //     { 'name': 'tagcloud', 'icon': 'cloud'}
  //   ];
  //   this.set('modes', modes);
  //   this.set('selectedMode', modes[1]);
  // },

  actions: {
    // change(mode) {
    //   debugger;
    //   this.set('selectedMode', mode)
    // }
    reload() {
      this.get('mountStatistics').perform();
    }
  }
});
