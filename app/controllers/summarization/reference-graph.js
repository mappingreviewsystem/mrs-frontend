import Controller from '@ember/controller';

export default Controller.extend({
  networkOptions: {
    autoResize: true,
    layout: {
      improvedLayout: false
    },
    nodes: {
      borderWidth: 1.0,
      font: {
          size: 14,
          face: "Tahoma",
          color: '#000'
      },
      scaling: {
        min: 10,
        max: 50,
        label: {
          min: 14,
          max: 30
        }
      },
      // chosen: {
      //   node: function(values, id, selected, hovering) {
      //     debugger;
      //     values.property = chosenValue;
      //
      //     // borderColor : "#5d9226" borderDashes : false borderRadius : 6 borderWidth : 1 color : "#82b74b" shadow : false shadowColor : "rgba(0,0,0,0.5)" shadowSize : 10 shadowX : 5 shadowY : 5 size : 50
      //   }
      // }
    },
    edges: {
      width: 0.2,
      hoverWidth: 3.0,
      color: {
        inherit: "to"
      },
      arrows: {
        to: {enabled: true, scaleFactor:1, type:'arrow'}
      },
      "smooth": {
        "type": "continuous",
        "forceDirection": "none"
      },
      selectionWidth: function(width) {return width*20;}
    },
    physics: {
      barnesHut: {
        gravitationalConstant: -20000,
        centralGravity: 0.3,
        springLength: 600,
        springConstant: 0.04,
        damping: 0.09,
        avoidOverlap: 0.8
      },
      // solver: 'forceAtlas2Based',
      // forceAtlas2Based: {
      //   gravitationalConstant: -500, // -50
      //   // centralGravity: 0.01, // 0.01
      //   // springLength: 100, // 100
      //   // springConstant: 0.08, // 0.08
      //   // damping: 0.4, // 0.4
      //   avoidOverlap: 0.1 // 0
      // },
      stabilization: {
        iterations: 2000
      }
    },
    interaction: {
      hideEdgesOnDrag: false,
      hoverConnectedEdges: true,
      multiselect: true,
      hover: true,
      navigationButtons: false,
      tooltipDelay: 200
    },
    manipulation: {
      enabled: false
    }
  }
});
