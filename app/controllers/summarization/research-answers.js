import Controller from '@ember/controller';
import { inject as service } from '@ember/service';
import { observer } from '@ember/object';
import { or, sort } from '@ember/object/computed';
import { task } from 'ember-concurrency';
import QueryParams from 'ember-parachute';

export const answersParams = new QueryParams({
  researchQuestionId: {
    defaultValue: undefined,
    refresh: true
  },
  articleId: {
    defaultValue: undefined,
    refresh: true
  }
});

export default Controller.extend(answersParams.Mixin, {
  currentUser: service(),
  store: service(),
  toastHandler: service(),

  queryParamsChanged: or('queryParamsState.{researchQuestionId,articleId}.changed'),

  setup({ queryParams }) {
    this.get('setupTask').perform(queryParams);
  },

  setupTask: task(function* (queryParams) {
    yield this.store.query('article', { filter: { status: ['extracted', 'selected'] } })
    .then(articles => { this.set('articles', articles); })

    yield this.store.findAll('research-question')
    .then(rq => { this.set('researchQuestions', rq); });

    this.fetchData(queryParams);
  }).restartable(),

  queryParamsDidChange({ shouldRefresh, queryParams }) {
    if (shouldRefresh) {
      this.fetchData(queryParams);
    }
  },

  reset({ queryParams }, isExiting) {
    if (isExiting) {
      this.resetQueryParams();
    }
  },

  fetchData(queryParams) {
    let article, researchQuestion;

    if (queryParams.articleId === undefined) article = this.get('articles').sortBy('title').get('firstObject');
    else article = this.get('articles').findBy('id', queryParams.articleId);
    this.set('article', article);

    if (queryParams.researchQuestionId === undefined) researchQuestion = this.get('researchQuestions').sortBy('title').get('firstObject');
    else researchQuestion = this.get('researchQuestions').findBy('id', queryParams.researchQuestionId);
    this.set('researchQuestion', researchQuestion);
  },

  sortFields: Object.freeze(['title']),
  sortedQuestions: sort('researchQuestions', 'sortFields'),
  sortedArticles: sort('articles', 'sortFields'),
  relatedExtractionQuestions: undefined,
  isPdfOpen: false,
  articleLoading: false,

  checkQuestions: observer('article.isPending', 'researchQuestion.isPending', function() {
    let article = this.get('article');
    let researchQuestion = this.get('researchQuestion');
    this.get('questionsTask').perform(article, researchQuestion);
  }),

  questionsTask: task(function* (article, researchQuestion) {
    if ((article === undefined) || (researchQuestion === undefined)) return;

    if (article.get('articleExtractionQuestions.isPending')) yield article.get('articleExtractionQuestions').reload();
    if (researchQuestion.get('extractionQuestions.isPending')) yield researchQuestion.get('extractionQuestions').reload();

    let response = {};
    yield researchQuestion.get('extractionQuestions').sortBy('key').forEach((extractionQuestion) => {
      let articleExtractionQuestion = article.get('articleExtractionQuestions').find((question) => { return question.belongsTo('extractionQuestion').id() === extractionQuestion.id; });
      if (articleExtractionQuestion === undefined) articleExtractionQuestion = this.get('store').createRecord('article-extraction-question', { extractionQuestion: extractionQuestion, article: article });
      response[extractionQuestion.get('key')] = articleExtractionQuestion;
    });
    this.set('relatedExtractionQuestions', response);
  }).restartable(),

  getModelIndex(model, sum) {
    let element = (model === 'article') ? this.get('article') : this.get('researchQuestion');
    let array = (model === 'article') ? this.get('sortedArticles') : this.get('sortedQuestions');
    let index = array.findIndex((el) => { return el.id === element.id });

    if (sum > 0) {
      if (index == (array.length - 1)) return -1;
    }
    else {
      if (index == 0) return -1;
    }

    this.set(`${model}Id`, array[index + sum].get('id'));
  },

  actions: {
    togglePdf() {
      this.toggleProperty('isPdfOpen');
    },

    next(model) {
      this.getModelIndex(model, 1);
    },
    previous(model) {
      this.getModelIndex(model, -1);
    },
    change(type, model) {
      if (model === null) return;
      this.set(type, model.id);
    },
    resetAll() {
      this.resetQueryParams();
    },

    refresh() {
      let article = this.get('article');
      this.set('articleLoading', true);
      article.get('articleExtractionQuestions').reload()
        .then(() => { this.set('articleLoading', false); })
        .catch((error) => {
          this.set('articleLoading', false);
          this.get('toastHandler').error(error);
        });
    },

    save(extractionQuestion, value) {
      if (value === '') {
        let researchQuestion = extractionQuestion.get('extractionQuestion');

        extractionQuestion.destroyRecord();

        let newQuestion = {
          article: this.get('article'),
          value: ''
        };
        newQuestion.extractionQuestion = researchQuestion;
        return new Promise(() => {
          this.get('store').createRecord('article-extraction-question', newQuestion);
        });
      }

      let oldValue = extractionQuestion.get('value');
      extractionQuestion.set('value', value);

      return extractionQuestion.save().then(() => { return true; })
      .catch((error) => {
        extractionQuestion.set('value', oldValue);
        this.get('toastHandler').error(error);
        return false;
      });
    }
  }
});
