import Controller from '@ember/controller';
import { inject as service } from '@ember/service';

export default Controller.extend({
  store: service(),
  currentUser: service(),

  actions: {
    new() {
      let user = this.get('currentUser.user');
      let group = this.get('store').createRecord('group');
      group.get('users').pushObject(user);
      group.save();
    }
  }
});
