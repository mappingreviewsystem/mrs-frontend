import Controller from '@ember/controller';
import { inject as service } from '@ember/service';
import { computed } from '@ember/object';

export default Controller.extend({
  notesSidenav: service(),
  currentUser: service(),

  isNew: computed('currentUser.currentResearch.isPending', 'currentUser.currentResearch.articlesCount', function() {
    if (this.get('currentUser.currentResearch.isPending')) return false;
    return (this.get('currentUser.currentResearch.articlesCount') < 1);
  }),

  init() {
    this._super(...arguments);
    this.set('status', []);
    this.set('tags', []);
    this.set('articleDatabases', []);
    this.set('inclusionCriteria', []);
    this.set('exclusionCriteria', []);
  },

  showProgressBar: computed('model.{currentPage,meta.total_pages}', function() {
    return (this.get('model.currentPage') < this.get('model.meta.total_pages'));
  }),

  filters: computed('status','inclusionCriteria','exclusionCriteria','tags', function() {
    return {
      status: this.get('status'),
      articleDatabases: this.get('articleDatabases'),
      inclusionCriteria: this.get('inclusionCriteria'),
      exclusionCriteria: this.get('exclusionCriteria'),
      tags: this.get('tags')
    };
  }),

  queryParams: ['search', 'status', 'tags', 'articleDatabases', 'inclusionCriteria', 'exclusionCriteria'],
  search: '',

  actions: {
    changeFilter(filter) {
      if (filter.value === undefined) return;

      if (this.get(filter.type).includes(filter.value)) {
        this.get(filter.type).removeObject(filter.value);
      }
      else {
        this.get(filter.type).addObject(filter.value);
      }
    },

    toggleSidenav(article) {
      if (article.get('notes.isPending')) article.reload();

      this.get('notesSidenav').toggle(article);
    },
  }
});
