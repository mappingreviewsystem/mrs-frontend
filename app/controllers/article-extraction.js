import Controller from '@ember/controller';
import { inject as service } from '@ember/service';
import { computed } from '@ember/object';
import { alias } from '@ember/object/computed';

export default Controller.extend({
  config: service(),
  notesSidenav: service(),
  currentUser: service(),

  selectedTab: 0,
  research: alias('currentUser.currentResearch.content'),
  sidenav: 'pdf',

  isSidenavOpen: true,
  pdfUrl: computed('model.fileUrl', function() {
    return this.get('model.fileUrl');
  }),

  actions: {
    toggleSidenav(sidenav) {
      if (this.get('sidenav') != sidenav) {
        this.set('sidenav', sidenav);
        this.set('isSidenavOpen', true);
      }
      else {
        this.toggleProperty('isSidenavOpen');
      }
    }
  }
});
