import Controller from '@ember/controller';
import { task, timeout } from 'ember-concurrency';

export default Controller.extend({
  selectedTab: 0,
  selectedResearchQuestion: undefined,
  isSidenavOpen: false,

  toggleTask: task(function*(researchQuestion) {
    this.set('isSidenavOpen', false);
    yield timeout(650);
    this.set('selectedResearchQuestion', researchQuestion);
    this.set('isSidenavOpen', true);
  }).restartable(),

  actions: {
    toggleSidenav(researchQuestion) {
      if (this.get('selectedResearchQuestion') == researchQuestion) {
        this.toggleProperty('isSidenavOpen');
      }
      else {
        this.get('toggleTask').perform(researchQuestion);
      }
    }
  }
});
