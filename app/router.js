import EmberRouter from '@ember/routing/router';
import config from './config/environment';
import RouterScroll from 'ember-router-scroll';

const Router = EmberRouter.extend(RouterScroll, {
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  this.route('login');

  // this.route('articles');
  this.route('article-extraction', { path: 'articles/:article_id/extraction' });
  this.route('researches');
  this.route('users');
  this.route('groups');
  // this.route('research');

  // TODO: implement devise routes
  // devise
  // this.route('sessions.new', {path: '/login'});
  // this.route('users.new', {path: '/signup'});
  // this.route('users.activation', {path: '/users/activation'});
  // this.route('users', function() {});
  // this.route('user', {path: '/users/:user_id'}, function() {
  //   this.route('edit');
  //   // this.route('following');
  //   // this.route('followers');
  // });
  this.route('notes');
  this.route('register');

  this.route('planning');
  this.route('execution', function() {
    this.route('selection');
    this.route('extraction');
    this.route('databases');
  });
  this.route('summarization', function() {
    this.route('statistics');
    this.route('research-answers');
    this.route('reference-graph');
  });
});

export default Router;
