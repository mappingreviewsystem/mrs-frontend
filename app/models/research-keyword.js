import DS from 'ember-data';

export default DS.Model.extend({
  research: DS.belongsTo('research'),
  keyword: DS.belongsTo('keyword')
});
