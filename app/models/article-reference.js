import DS from 'ember-data';

export default DS.Model.extend({
  source: DS.belongsTo('article'),
  destination: DS.belongsTo('article'),
  status: DS.attr('number')
});
