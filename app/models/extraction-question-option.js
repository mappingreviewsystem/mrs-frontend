import DS from 'ember-data';

export default DS.Model.extend({
  title: DS.attr('string'),
  extractionQuestion: DS.belongsTo('extraction-question'),
  articleExtractionQuestions: DS.hasMany('article-extraction-question', { inverse: 'extractionQuestionOptions' }),
  articles: DS.hasMany('article')
});
