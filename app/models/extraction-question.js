import DS from 'ember-data';
import { computed } from '@ember/object';

export default DS.Model.extend({
  key: DS.attr('string'),
  value: DS.attr('string'),
  research: DS.belongsTo('research'),
  researchQuestions: DS.hasMany('research-question'),
  extractionQuestionOptions: DS.hasMany('extraction-question-option'),
  articleExtractionQuestions: DS.hasMany('article-extraction-question'),
  articles: DS.hasMany('article'),
  fieldType: DS.attr('string'),
  rangeMin: DS.attr('number'),
  rangeMax: DS.attr('number'),
  rangeIncrement: DS.attr('number'),

  fieldTypes: ['text', 'range', 'range_radio', 'radiobox', 'selector', 'checkbox'],

  formAttributes: function(_action) {
    return {
      "key": "text",
      "value": "textarea",
    };
  },

  numberType: computed('fieldType', function() {
    let fieldType = this.get('fieldType');
    return ['range', 'range_radio'].includes(fieldType);
  }),

  listType: computed('fieldType', function() {
    let fieldType = this.get('fieldType');
    return ['radiobox', 'selector', 'checkbox'].includes(fieldType);
  }),
});
