import DS from 'ember-data';

export default DS.Model.extend({
  article: DS.belongsTo('article'),
  exclusionCriterium: DS.belongsTo('exclusion-criterium'),
  meet: DS.attr('boolean', { defaultValue: false }),
  doubt: DS.attr('number'),

  formAttributes: function(_action) {
    return {
      "meet": "checkbox"
    };
  },
});
