import DS from 'ember-data';

export default DS.Model.extend({
  article: DS.belongsTo('article'),
  inclusionCriterium: DS.belongsTo('inclusion-criterium'),
  meet: DS.attr('boolean', { defaultValue: false }),
  doubt: DS.attr('number'),

  formAttributes: function(_action) {
    return {
      "meet": "checkbox"
    };
  },
});
