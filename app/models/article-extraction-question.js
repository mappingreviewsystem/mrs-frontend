import DS from 'ember-data';

export default DS.Model.extend({
  article: DS.belongsTo('article'),
  extractionQuestion: DS.belongsTo('extraction-question'),
  extractionQuestionOption: DS.belongsTo('extraction-question-option', { inverse: null }),
  extractionQuestionOptions: DS.hasMany('extraction-question-option', { inverse: 'articleExtractionQuestions' }),
  value: DS.attr('string'),
  doubt: DS.attr('number'),

  formAttributes: function(_action) {
    return {
      "value": "pell"
    };
  },
});
