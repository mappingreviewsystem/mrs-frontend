import DS from 'ember-data';

export default DS.Model.extend({
  title: DS.attr('string'),
  value: DS.attr('string'),
  color: DS.attr('string', { defaultValue: 'yellow' }),
  priority: DS.attr('number'),
  article: DS.belongsTo('article'),
  research: DS.belongsTo('research'),
  tags: DS.hasMany('tag'),

  formAttributes: function(_action) {
    return {
      "title": "text",
      "value": "pell",
    };
  },
});
