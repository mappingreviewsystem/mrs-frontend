import DS from "ember-data";

export default DS.Model.extend({
  title: DS.attr("string"),
  description: DS.attr("string"),
  value: DS.attr("string"),
  research: DS.belongsTo("research"),
  extractionQuestions: DS.hasMany("extraction-question"),

  formAttributes: function(_action) {
    return {
      "title": "text",
      "description": "textarea",
      "value": "pell",
    };
  },
});
