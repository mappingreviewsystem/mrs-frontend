import DS from 'ember-data';

export default DS.Model.extend({
  label: DS.attr('string'),
  color: DS.attr('string'),
  research: DS.belongsTo('research'),
  articles: DS.hasMany('article'),
  notes: DS.hasMany('note'),
});
