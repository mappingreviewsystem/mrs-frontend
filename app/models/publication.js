import DS from 'ember-data';

export default DS.Model.extend({
  title: DS.attr('string'),
  publicationType: DS.attr('string'),
  location: DS.attr('string'),
  issn: DS.attr('string'),
  isbn: DS.attr('string'),
  publisher: DS.belongsTo('publisher'),
  research: DS.belongsTo('research'),
  articles: DS.hasMany('article'),

  // model form attributes
  formAttributes: function(_action) {
    return {
      "title": 'text',
      "publicationType": 'text',
      "location": 'text',
      "issn": 'text',
      "isbn": 'text',
    };
  },
});
