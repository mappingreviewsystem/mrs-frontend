import DS from 'ember-data';
import { computed } from '@ember/object';

export default DS.Model.extend({
  name: DS.attr('string'),
  email: DS.attr('string'),
  encryptedPassword: DS.attr('string'),
  resetPasswordToken: DS.attr('string'),
  resetPasswordSentAt: DS.attr('date'),
  rememberCreatedAt: DS.attr('date'),
  signInCount: DS.attr('number'),
  currentSignInAt: DS.attr('date'),
  lastSignInAt: DS.attr('date'),
  currentSignInIp: DS.attr('string'),
  lastSignInIp: DS.attr('string'),
  role: DS.attr('number'),
  researches: DS.hasMany('research', { inverse: 'user' }),
  groupResearches: DS.hasMany('research', { inverse: null }),
  currentResearch: DS.belongsTo('research', { inverse: null }),
  groupMemberships: DS.hasMany('group-membership'),
  groups: DS.hasMany('group', { inverse: 'users' }),

  // for registration
  password: DS.attr('string'),
  passwordConfirmation: DS.attr('string'),

  // merge researches and groupResearches
  allResearches: computed('researches.isPending', 'groupResearches.isPending', function() {
    let researches = this.get('researches');
    let groupResearches = this.get('groupResearches');
    if (researches.get('isPending') || groupResearches.get('isPending')) return [];

    let response = researches.toArray()
    response.pushObjects(groupResearches.toArray());
    return response.filter((v, i, self) => { return self.indexOf(v) === i; }).sortBy('title');
  }),
});
