import DS from 'ember-data';

export default DS.Model.extend({
  article: DS.belongsTo('article'),
  value: DS.attr('string'),
  page: DS.attr('number')
});
