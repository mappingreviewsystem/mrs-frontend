import DS from 'ember-data';
import { memberAction } from 'ember-api-actions';

export default DS.Model.extend({
  title: DS.attr('string'),
  url: DS.attr('string'),
  query: DS.attr('string'),
  exportedSearch: DS.attr('string'),
  exportedSearchUpdatedAt: DS.attr('date'),
  status: DS.attr('string'),
  downloaded: DS.attr('boolean'),
  missingLines: DS.attr('string'),
  createdAt: DS.attr('date'),
  updatedAt: DS.attr('date'),
  research: DS.belongsTo('research'),
  articles: DS.hasMany('article'),

  formAttributes: function(_action) {
    return {
      "title": "text",
      "query": "text",
      "url": "text",
    };
  },

  import: memberAction({
    path: 'import',
    type: 'get', // HTTP POST request
    urlType: 'findRecord' // Base of the URL that's generated for the action
  }),
});
