import DS from 'ember-data';

export default DS.Model.extend({
  title: DS.attr('string'),
  articles: DS.hasMany('article'),
  researches: DS.hasMany('research'),

  formAttributes: function(_action) {
    return {
      "title": "chips"
    };
  },
});
