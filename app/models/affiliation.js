import DS from 'ember-data';

export default DS.Model.extend({
  name: DS.attr('string'),
  location: DS.attr('string'),
  authors: DS.hasMany('author'),
  research: DS.belongsTo('research')
});
