import DS from 'ember-data';
import { inject as service } from '@ember/service';
import { computed } from '@ember/object';

export default DS.Model.extend({
  currentUser: service(),

  name: DS.attr('string'),
  description: DS.attr('string'),
  researches: DS.hasMany('research'),
  groupMemberships: DS.hasMany('group-membership'),
  users: DS.hasMany('user'),
  owners: DS.hasMany('user', { inverse: null }),

  isCurrentUserOwner: computed('owners.isPending', 'currentUser.user.isPending', function() {
    if (this.get('owners.isPending') || this.get('currentUser.user.isPending')) return false;

    let user = this.get('currentUser.user');
    let owner = this.get('owners').find(owner => { return owner.id == user.id });
    return (owner !== undefined);
  }),

  formAttributes: function(_action) {
    return {
      "name": 'text',
      "description": 'text',
    };
  },
});
