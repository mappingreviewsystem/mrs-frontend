import DS from 'ember-data';
import { memberAction, collectionAction } from 'ember-api-actions';
import { mapBy } from '@ember/object/computed';

export default DS.Model.extend({
  title: DS.attr('string'),
  objective: DS.attr('string'),
  articlesCount: DS.attr('number'),
  user: DS.belongsTo('user', { inverse: 'researches' }),
  // currentUser: DS.belongsTo('user', { inverse: 'currentResearch' }),
  group: DS.belongsTo('group'),
  keywords: DS.hasMany('keyword'),
  articles: DS.hasMany('article'),
  selectedArticles: DS.hasMany('article'),
  // articleReferences: DS.hasMany('article-reference'),
  // tags: DS.hasMany('tag'),
  articleDatabases: DS.hasMany('article-database'),
  researchQuestions: DS.hasMany('research-question'),
  extractionQuestions: DS.hasMany('extraction-question'),
  inclusionCriteria: DS.hasMany('inclusion-criterium'),
  exclusionCriteria: DS.hasMany('exclusion-criterium'),
  notes: DS.hasMany('note'),
  tags: DS.hasMany('tag'),

  // keywordList: mapBy('keywords', 'title'),
  keywordList: DS.attr('string'),

  formAttributes: function(_action) {
    return {
      "title": "text",
      "objective": "textarea"
    };
  },

  /* statistics route */
  statistics: memberAction({
    path: 'statistics',
    type: 'get'
  }),

  /* statistics route */
  spreadsheet: memberAction({
    type: 'get',
    path: 'spreadsheet?base64=true', // base64 is a hack to cope with $.ajax, remove when possible
    ajaxOptions: {
      arraybuffer: true
    }
  }),

  /* reference graph attributes */
  // route
  referenceGraphs: collectionAction({
    path: 'reference_graphs',
    type: 'get', // HTTP POST request
    // urlType: 'findRecord' // Base of the URL that's generated for the action
  }),
});
