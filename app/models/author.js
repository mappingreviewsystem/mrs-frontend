import DS from 'ember-data';
import { computed } from '@ember/object';

export default DS.Model.extend({
  name: DS.attr('string'),
  articles: DS.hasMany('article'),
  affiliations: DS.hasMany('affiliation'),
  research: DS.belongsTo('research'),

  firstAffiliation: computed('affiliations', function() {
    let affiliations = this.get('affiliations');
    if (affiliations.length > 0) {
      affiliations.get('firstObject').get('name');
    }
    return '';
  }),

  // model form attributes
  formAttributes: function(_action) {
    return {
      "name": 'text',
      "affiliations": 'select',
    };
  },
});
