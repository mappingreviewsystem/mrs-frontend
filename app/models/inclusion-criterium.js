import DS from 'ember-data';

export default DS.Model.extend({
  title: DS.attr('string'),
  research: DS.belongsTo('research'),
  articles: DS.hasMany('article'),

  formAttributes: function(_action) {
    return {
      "title": "text"
    };
  },
});
