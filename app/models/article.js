import DS from 'ember-data';
import { memberAction } from 'ember-api-actions';
import { computed } from '@ember/object';
import { mapBy } from '@ember/object/computed';
import { inject as service } from '@ember/service';

export default DS.Model.extend({
  config: service(),

  title: DS.attr('string'),
  url: DS.attr('string'),
  publicationYear: DS.attr('number'),
  doi: DS.attr('string'),
  volume: DS.attr('string'),
  issue: DS.attr('string'),
  pageStart: DS.attr('string'),
  pageEnd: DS.attr('string'),
  isbn: DS.attr('string'),
  issn: DS.attr('string'),
  status: DS.attr('string'),
  firstAuthor: DS.attr('string'),
  fileFileName: DS.attr('string'),
  fileContentType: DS.attr('string'),
  fileFileSize: DS.attr('number'),
  fileUpdatedAt: DS.attr('date'),
  file: DS.attr('string'),
  referencedByCount: DS.attr('number'),
  referencesCount: DS.attr('number'),
  articleDatabase: DS.belongsTo('article-database'),
  research: DS.belongsTo('research', { inverse: 'articles' }),
  publication: DS.belongsTo('publication'),
  publisher: DS.belongsTo('publisher'),
  contentType: DS.belongsTo('content-type'),
  notes: DS.hasMany('note'),
  tags: DS.hasMany('tag'),
  authors: DS.hasMany('author'),
  keywords: DS.hasMany('keyword'),
  articlePages: DS.hasMany('article-page'),
  inclusionCriteria: DS.hasMany('inclusion-criterium'),
  articleInclusionCriteria: DS.hasMany('article-inclusion-criterium'),
  exclusionCriteria: DS.hasMany('exclusion-criterium'),
  articleExclusionCriteria: DS.hasMany('article-exclusion-criterium'),
  extractionQuestions: DS.hasMany('extraction-question'),
  articleExtractionQuestions: DS.hasMany('article-extraction-question'),
  referencedBy: DS.hasMany('article', { inverse: 'references' }),
  references: DS.hasMany('article', { inverse: 'referencedBy' }),

  // variables to create content only
  // publicationTitle: DS.attr('string'),
  authorList: mapBy('authors', 'name'),
  keywordList: mapBy('keywords', 'title'),

  fileUrl: computed('file', function() {
    let file = this.get('file');
    if (!file) return undefined;
    return `${this.get('config.api.host')}${file}`;
  }),

  // model form attributes
  formAttributes: function(_action) {
    return {
      "title": 'text',
      "contentType": 'select',
      "url": 'text',
      "publicationYear": 'number',
      "doi": 'text',
      "volume": 'text',
      "issue": 'text',
      "pageStart": 'text',
      "pageEnd": 'text',
      "isbn": 'text',
      "issn": 'text',
      "status": 'text',
    };
  },

  isSelected: computed('status', function() {
    let status = this.get('status');
    return ((status == "selected") || (status == "extracted"));
  }),

  /* reference graph attributes */
  // route
  referenceGraphs: memberAction({
    path: 'reference_graphs',
    type: 'get',
    urlType: 'findRecord' // Base of the URL that's generated for the action
  }),

  crossref: memberAction({
    path: 'crossref',
    type: 'get',
    urlType: 'findRecord' // Base of the URL that's generated for the action
  }),
});
