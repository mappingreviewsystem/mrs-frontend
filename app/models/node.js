import DS from 'ember-data';

export default DS.Model.extend({
  article: DS.belongsTo('article'),
  title: DS.attr('string'),
  label: DS.attr('string'),
  shape: DS.attr('string'),
  color: DS.attr('string'),
  value: DS.attr('string'),
  mass: DS.attr('string'),
  level: DS.attr('string'),
  hidden: DS.attr('boolean'),
  relevant: DS.attr('boolean')
  // referenced: computed('referencedBies', 'status', function() {
  //   if (this.get('status') != 'not_included') return true;
  //   let id = this.id;
  //   this.store.query('article-reference', {
  //     filter: {
  //       destination: { id: id }
  //     }
  //   }).then(function(references) {
  //     // debugger;
  //     return references.lenght > 1;
  //   });
  // })
});
