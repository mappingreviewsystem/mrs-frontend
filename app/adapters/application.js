import DS from 'ember-data';
import DataAdapterMixin from 'ember-simple-auth/mixins/data-adapter-mixin';
import { inject as service } from '@ember/service';
import { alias } from '@ember/object/computed';
import { decamelize, underscore } from '@ember/string';
import { pluralize } from 'ember-inflector';

export default DS.JSONAPIAdapter.extend(DataAdapterMixin, {
  config: service(),
  session: service(),

  host: alias('config.api.host'),
  namespace: alias('config.api.namespace'),

  ajaxOptions: function(url, type, options) {
    var hash = this._super(url, type, options);

    // TODO: hack to cope with lack of support for binary responses in $.ajax
    if (typeof options === 'object' && options.arraybuffer === true) {
      hash.dataType = 'text';
    }
    return hash;
  },

  // authorizer: 'authorizer:devise',
  authorize(xhr) {
    let data = this.get('session.data.authenticated');

    let now = (new Date()).getTime();
    if (data.expiry * 1000 > now) {
      xhr.setRequestHeader('access-token', data.accessToken);
      xhr.setRequestHeader('expiry', data.expiry);
      xhr.setRequestHeader('token-type', data.tokenType);
      xhr.setRequestHeader('uid', data.uid);
      xhr.setRequestHeader('client', data.client);
    }
  },
  pathForType(type) {
    var decamelized = decamelize(type);
    var underscored = underscore(decamelized);
    return pluralize(underscored);
  },

  /* handle token changes for devise_token_auth */
  handleResponse(status, headers, payload, requestData) {
    this.ensureResponseAuthorized(status, headers, payload, requestData);

    if (headers.expiry) {
      let session = this.get('session');
      if (session.session.content.authenticated.expiry < headers.expiry) {
        session.set('data.authenticated.expiry', headers['expiry']);
        session.set('data.authenticated.client', headers['client']);
        session.set('data.authenticated.accessToken', headers['access-token']);

        // persist to storage hack
        // https://github.com/simplabs/ember-simple-auth/issues/926
        let data = this.get('session.data');
        this.get('session.store').persist(data);
      }
    }

    return this._super(...arguments);
  }
});
