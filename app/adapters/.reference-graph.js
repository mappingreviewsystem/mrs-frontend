import GraphQLAdapter from 'ember-graphql-adapter';
import DataAdapterMixin from 'ember-simple-auth/mixins/data-adapter-mixin';

export default GraphQLAdapter.extend(DataAdapterMixin, {
  httpMethod: 'POST',
  endpoint: 'http://localhost:3000/graphql',

  /* handle token changes for devise_token_auth */
  authorizer: 'authorizer:devise',
  handleResponse(status, headers, payload, requestData) {
    this.ensureResponseAuthorized(status, headers, payload, requestData);

    let _session = this.get('session');
    if (_session.session.content.authenticated.expiry < headers.expiry) {
      _session.set('data.authenticated.expiry', headers['expiry']);
      _session.set('data.authenticated.client', headers['client']);
      _session.set('data.authenticated.accessToken', headers['access-token']);
    }

    return this._super(...arguments);
  }
});
