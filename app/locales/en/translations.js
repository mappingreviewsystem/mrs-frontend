export default {
  // "some.translation.key": "Text for some.translation.key",
  //
  // "a": {
  //   "nested": {
  //     "key": "Text for a.nested.key"
  //   }
  // },
  //
  // "key.with.interpolation": "Text with {{anInterpolation}}"

  "article": {
    "title": "Title",
    "publicationYear": "Year",
    "tags": "Tags",
    "publisher": "Publisher",
    "status": "Status",
    "status.extracted": "Extracted",
    "status.selected": "Selected",
    "status.unseen": "Unseen",
    "status.excluded": "Excluded",
    "status.not_included": "Not included",
  },
  "research-question.title": "Title",
  "research-question.description": "Description",
};
