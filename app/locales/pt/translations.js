export default {
  // "some.translation.key": "Text for some.translation.key",
  //
  // "a": {
  //   "nested": {
  //     "key": "Text for a.nested.key"
  //   }
  // },
  //
  // "key.with.interpolation": "Text with {{anInterpolation}}"
  // inputs
  "input.blank": "** em branco **",
  "input.answer": "Resposta",
  "input.notAnswered": "Não respondido",

  // errors
  "adapter-error": "Não foi possível se comunicar com o servidor",
  "unknown-error": "Erro desconhecido",

  // controls
  "controls": {
    "back": "Voltar",
    "continue": "Continuar",
    "finish": "Finalizar"
  },

  // pdf.js
  "pdf": {
    "zoom": {
      "auto": "Zoom automático",
      "page-actual": "Tamanho real",
      "page-fit": "Ajustar à janela",
      "page-width": "Largura da página",
      "0.5": "50%",
      "0.75": "75%",
      "1": "100%",
      "1.25": "125%",
      "1.5": "150%",
      "2": "200%",
      "3": "300%",
      "4": "400%"
    }
  },

  // search filters
  "search-filters": {
    "status": "Estado",
    "tags": "Tags",
    "articleDatabases": "Banco de dados",
    "articleDatabases.none": "Sem banco de dados",
    "inclusionCriteria": "Critérios de Inclusão",
    "exclusionCriteria": "Critérios de Exclusão",
    "addFilter": "Adicionar filtro",
  },

  // search
  "search": {
    "no-results": "Nenhum resultado."
  },

  // models' attributes
  "selection-criteria": "Critérios de Seleção",
  "content-type": {
    "journal-article": "Artigo de Revista",
    "proceedings-article": "Artigo de Evento",
    "book-chapter": "Capítulo de Livro",
    "book": "Livro",
    "journal": "Revista",
    "monograph": "Monografia",
    "dissertation": "Dissertação",
    "dataset": "Conjunto de Dados",
    "other": "Outro",
    // TODO: translate to pt-br
    "book-section": "Book Section",
    "report": "Report",
    "book-track": "Book Track",
    "book-part": "Part",
    "journal-volume": "Journal Volume",
    "book-set": "Book Set",
    "reference-entry": "Reference Entry",
    "component": "Component",
    "report-series": "Report Series",
    "proceedings": "Proceedings",
    "standard": "Standard",
    "reference-book": "Reference Book",
    "posted-content": "Posted Content",
    "journal-issue": "Journal Issue",
    "book-series": "Book Series",
    "edited-book": "Edited Book",
    "standard-series": "Standard Series"
  },
  "article": {
    "createRecord": "Adicionar Artigo",
    "model": "Artigo",
    "models": "Artigos",
    "title": "Título",
    "publicationYear": "Ano",
    "contentType": "Tipo",
    "url": "URL",
    "doi": "DOI",
    "volume": "Volume",
    "issue": "Edição",
    "pageStart": "Página inicial",
    "pageEnd": "Página final",
    "isbn": "ISBN",
    "issn": "ISSN",
    "importedFrom": "Importado de",
    "importedFromNothing": "Avulso (Sem base)",
    "authors": "Autores",
    "keywords": "Palavras-chave",
    "tags": "Tags",
    "publisher": "Publicador",
    "status": "Estado",
    "status.extracted": "Extraído",
    "status.selected": "Selecionado",
    "status.unseen": "Não visto",
    "status.excluded": "Excluído",
    "status.not_included": "Não incluído",
    "import-metadata-confirmation": "Você deseja atualizar os metadados do seu documento por meio do serviço Crossref.org ?",
    "import-metadata-references-confirmation": "Você deseja atualizar os metadados do seu artigo e suas referências por meio do serviço Crossref.org ?\n(As referências serão agendadas e podem demorar um tempo para importar ou refletir as mudanças)",
    "new-metadata-info": "Os outros metadados podem ser inseridos na visualização do artigo, após sua inserção.<br>¹ Artigos que possuem o DOI serão importados do serviço Crossref.org"
  },
  "article-reference": {
    "model": "Referência",
    "models": "Referências",
    "createRecord": "Criar Referência",
    "destroyRecord": "Remover referência",
    "destroyRecordQuestion": "Remover referência?",
    "blankRecord": "Referência em branco!"
  },
  "group": {
    "model": "Grupo",
    "models": "Grupos",
    "name": "Nome",
    "description": "Descrição",
    "owner": "Dono",
    "member": "Membro",
    "members": "Membros"
  },
  "group-membership": {
    "createRecord": "Adicionar usuário",
    "destroyRecordQuestion": "Remover membro?",
    "owner": "Dono",
    "member": "Membro",
    "chooseMember": "Escolha um membro ou cancele a ação."
  },
  "keyword": {
    "model": "Palavra-chave",
    "models": "Palavras-chave",
    "title": "Palavra-chave"
  },
  "author": {
    "model": "Autor",
    "models": "Autores",
    "name": "Nome",
    "affiliations": "Vinculações",
  },
  "affiliation": {
    "model": "Vinculação",
    "models": "Vinculações",
    "name": "Nome",
    "location": "Localização"
  },
  "note": {
    "model": "Nota",
    "models": "Notas",
    "title": "Título",
    "value": "Conteúdo",
    "quick-new": "Nota Rápida"
  },
  "research-question": {
    "createRecord": "Criar Pergunta de Pesquisa",
    "model": "Pergunta de Pesquisa",
    "models": "Perguntas de Pesquisa",
    "title": "Título",
    "description": "Descrição",
    "value": "Resposta",
    "pendingValue": "Resposta não preenchida.",
    "relatedExtractionQuestions": "Perguntas de Extração relacionadas"
  },
  "inclusion-criterium": {
    "createRecord": "Criar Critério de Inclusão",
    "model": "Critério de Inclusão",
    "models": "Critérios de Inclusão",
    "title": "Título"
  },
  "exclusion-criterium": {
    "createRecord": "Criar Critério de Exclusão",
    "model": "Critério de Exclusão",
    "models": "Critérios de Exclusão",
    "title": "Título"
  },
  "extraction-question": {
    "createRecord": "Criar Pergunta de Extração",
    "model": "Pergunta de Extração",
    "models": "Perguntas de Extração",
    "key": "Título",
    "value": "Descrição",
    "rangeMin": "Mínimo",
    "rangeMax": "Máximo",
    "rangeIncrement": "Incremento",
    "fieldType": "Tipo de Resposta",
    "fieldTypes": {
      "text": "Texto",
      "range": "Numeral",
      "range_radio": "Escala",
      "radiobox": "Itens de única escolha",
      "selector": "Lista de única escolha",
      "checkbox": "Itens de múltipla escolha",
    },
    "relatedResearchQuestions": "Perguntas de Pesquisa relacionadas"
  },
  "extraction-question-option": {
    "createRecord": "Criar nova opção",
    "newRecord": "Nova opção",
    "model": "Opção",
    "models": "Opções",
    "title": "Título"
  },
  "research": {
    "model": "Pesquisa",
    "models": "Pesquisas",
    "destroyRecordQuestion": "Remover a pesquisa?\nDigite o título dela para confirmar:",
    "title": "Título",
    "objective": "Objetivo",
    "group": "Grupo",
    "owner": "Dono"
  },
  "article-database": {
    "createRecord": "Dê um título para a base de dados para continuar.",
    "title": "Título",
    "url": "URL",
    "query": "String de busca",
    "status": {
      "not_imported": "Não importado",
      "importing": "Importando",
      "imported": "Importado"
    }
  },
  "publication": {
    "model": "Publicação",
    "models": "Publicações",
    "title": "Título",
    "issn": "ISSN",
    "isbn": "ISBN"
  }
};
