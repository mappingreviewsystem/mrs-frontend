import Route from '@ember/routing/route';
import InfinityRoute from "ember-infinity/mixins/route";
import { inject as service } from '@ember/service';

export default Route.extend(InfinityRoute, {
  currentUser: service(),

  queryParams: {
    search: {
      refreshModel: true
    },
    status: {
      refreshModel: true
    },
    tags: {
      refreshModel: true
    },
    articleDatabases: {
      refreshModel: true
    },
    inclusionCriteria: {
      refreshModel: true
    },
    exclusionCriteria: {
      refreshModel: true
    },
  },

  model(params) {
    let search = params.search
    if (search === undefined) search = "";
    return this.infinityModel("article", {
      search,
      filter: {
        status: params.status,
        article_database_id: params.articleDatabases,
        inclusion_criterium_ids: params.inclusionCriteria,
        exclusion_criterium_ids: params.exclusionCriteria,
        tag_ids: params.tags,
      },
      perPage: 10,
      startingPage: 1
    });
  },

  setupController(controller, model) {
    this._super(controller, model);
    this.get('currentUser').reloadResearch();
  },

});
