import Route from '@ember/routing/route';
import ApplicationRouteMixin from 'ember-simple-auth/mixins/application-route-mixin';
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';
import { inject as service } from '@ember/service';

export default Route.extend(ApplicationRouteMixin, AuthenticatedRouteMixin, {
  store: service(),
  session: service('session'),
  currentUser: service(),
  i18n: service(),
  pageTitleList: service('page-title-list'),
  routeAfterAuthentication: 'index',

  isSidenavOpen: false,
  sidenavModel: undefined,
  pageHeader: undefined,

  beforeModel() {
    return this._loadCurrentUser();
  },

  sessionAuthenticated() {
    this._super(...arguments);
    this._loadCurrentUser();
  },

  _loadCurrentUser() {
    return this.get('currentUser').load().catch(() => this.get('session').invalidate())
  },

  actions: {
    logout() {
      this.get('session').invalidate();
    },
    loading(transition, originRoute) {
      let controller = this.controllerFor('application');
      controller.set('isLoadingRoute', true);
      transition.promise.finally(() => {
        controller.set('isLoadingRoute', false);
      });
    }
  }
});
