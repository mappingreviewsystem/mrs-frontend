import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';
import { alias } from '@ember/object/computed';

export default Route.extend({
  store: service(),
  currentUser: service(),
  research: alias('currentUser.currentResearch'),

  model() {
    let research = this.get('research').content;
    return research.referenceGraphs().then(response => {
      return {
         nodes: this.get('store').push(response.nodes),
         articleReferences: this.get('store').push(response.edges)
      };
    });
  }
});
