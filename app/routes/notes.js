import Route from '@ember/routing/route';

export default Route.extend({
  model() {
    let research = this.get('store').findRecord('research', this.get('currentUser').user.get('currentResearch'));
    return research.notes;
  }
});
