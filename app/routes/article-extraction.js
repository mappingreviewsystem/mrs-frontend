import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';
import RSVP from 'rsvp';

export default Route.extend({
  currentUser: service(),

  model(params) {
    return this.get('store').findRecord('article', params.article_id);
  }
});
