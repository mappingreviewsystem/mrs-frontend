import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';

export default Route.extend({
  session: service(),
  currentUser: service(),

  beforeModel() {
    if (this.get('session.isAuthenticated') == true) {
      this._loadCurrentUser().then(() => {
        // when the user is new (or don't have a currentResearch),
        // create a new research and redirect/transition to the guide
        if (this.get('currentUser.user').belongsTo('currentResearch').id() === null) {
          let user = this.get('currentUser.user');
          let research = this.get('store').createRecord('research', {
            title: "Nova pesquisa",
            user
          });
          user.set('currentResearch', research);
          research.save().then(() => {
            user.save();
            this.transitionTo('planning');
          });
        }
        else {
          this.transitionTo('planning');
        }
      });
    }
    else {
      this.transitionTo('login');
    }
  },

  _loadCurrentUser() {
    return this.get('currentUser').load().catch(() => this.get('session').invalidate());
  }
});
