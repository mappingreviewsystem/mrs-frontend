import Service from '@ember/service';
import { task, timeout } from 'ember-concurrency';

export default Service.extend({
  isOpen: false,
  model: undefined,

  toggle(model) {
    if (this.get('model') == model) {
      this.toggleProperty('isOpen');
    }
    else {
      this.get('toggleTask').perform(model);
    }
  },

  close() {
    this.set('isOpen', false);
  },

  toggleTask: task(function*(model) {
    this.set('isOpen', false);
    yield timeout(650);
    this.set('model', model);
    this.set('isOpen', true);
  }).restartable(),
});
