import Service from '@ember/service';
import { inject as service } from '@ember/service';
import { alias } from '@ember/object/computed';
import { isEmpty } from '@ember/utils';
import RSVP from 'rsvp';

export default Service.extend({
  session: service(),
  store: service(),

  user: undefined,
  currentResearch: alias('user.currentResearch'),

  load() {
    let userId = this.get('session.data.authenticated.account.id');
    if (!isEmpty(userId)) {
      // load all reasearches (for research-selector)
      this.get('store').findAll('research');

      return this.get('store').find('user', userId).then((user) => {
        this.set('user', user);
      });
    } else {
      return RSVP.resolve();
    }
  },

  reloadResearch() {
    let currentResearch = this.get('currentResearch');
    if (!isEmpty(currentResearch)) {
      return this.get('store').findRecord('research', currentResearch.content.id);
    }
  }
});
