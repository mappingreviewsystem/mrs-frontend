import Service from '@ember/service';
import { inject as service } from '@ember/service';

export default Service.extend({
  paperToaster: service(),
  i18n: service(),

  error(error, options) {
    if (options === undefined) options = { duration: 4000, toastClass: 'md-warn' };
    let errorText = "";

    if (Ember.typeOf(error) === 'string') errorText = error;
    else {
      if (error.hasOwnProperty('isAdapterError') && error.isAdapterError) {
        errorText = this.get('i18n').t('adapter-error');
      }
      else {
        errorText = this.get('i18n').t('unknown-error');
      }
    }

    this.get('paperToaster').show(errorText, options);
  }
});
