import Service from '@ember/service';
import { task } from 'ember-concurrency';
import { inject as service } from '@ember/service';
// import adapter from '../adapters/application';

export default Service.extend({
  config: service(),
  session: service(),
  i18n: service(),
  toastHandler: service(),

  upload: function(file, fileKey, url, method) {
    return this.get('uploadTask').perform(file, fileKey, url, method);
  },

  uploadTask: task(function * (file, fileKey, url, method = "POST") {
    url = this.formatUrl(url);
    let headers = this.get('session.session.content.authenticated');
    let response = yield file.upload(url, {
      method: method,
      fileKey: fileKey,
      headers: {
        uid: headers.uid,
        client: headers.client,
        "token-type": headers.tokenType,
        "access-token": headers.accessToken,
        expiry: headers.expiry
      }
    }).catch((error) => {
      if (error.status == 0) error = this.get('i18n').t('adapter-error').string;
      this.get('toastHandler').error(error);
    });

    // update auth headers
    // TODO: find a way to import handleResponse from default adapter (now is a copy of this function)
    // adapter.handleResponse(response.status, response.headers, response.body);
    headers = response.headers;
    if (headers.expiry) {
      let session = this.get('session');
      if (session.session.content.authenticated.expiry < headers.expiry) {
        session.set('data.authenticated.expiry', headers['expiry']);
        session.set('data.authenticated.client', headers['client']);
        session.set('data.authenticated.accessToken', headers['access-token']);
        let data = this.get('session.data');
        this.get('session.store').persist(data);
      }
    }

    return response;
  }).maxConcurrency(3).enqueue(),

  formatUrl: function(url) {
    let regex = /^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([-.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/gm;
    if (regex.test(url)) return url;
    let divider = (/^\/.*$/.test(url)) ? '' : '/';
    return `${this.get('config.api.host')}/${this.get('config.api.namespace')}${divider}${url}`;
  }
});
