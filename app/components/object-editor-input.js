import Component from '@ember/component';
import { A } from '@ember/array';
import { computed } from '@ember/object';
import { inject as service } from '@ember/service';

export default Component.extend({
  store: service(),
  toastHandler: service(),

  isEdit: false,
  isEditing: false,

  model: undefined,
  attribute: undefined,
  values: A([]),
  tempValue: undefined,

  label: computed('model', 'attribute', function() {
    let model = this.get('model');
    if (model === undefined) return '';
    return `${model.constructor.modelName}.${this.get('attribute')}`;
  }),

  type: computed('model', 'attribute', function() {
    let model = this.get('model');
    if (model === undefined) return 'text';
    return model.formAttributes()[this.get('attribute')];
  }),

  value: computed('model', 'attribute', 'tempValue', function() {
    let value = this.get('tempValue');
    if (value !== undefined) return value;

    let attribute = this.get(`model.${this.get('attribute')}`);
    if ((attribute === undefined) || (attribute === null)) return '';
    return attribute;
  }),

  didReceiveAttrs() {
    this._super(...arguments);

    if (this.get('type') === 'select') {
      // TODO: find a better way to dasherize
      let attribute = this.get('attribute').replace(/([a-zA-Z])(?=[A-Z])/g, '$1-').toLowerCase();
      this.set('values', this.get('store').findAll(attribute));
    }
    this.set('oldValue', this.get('value'));
  },


  didRender() {
    this._super(...arguments);
    if (this.get('isEdit') && !this.get('isEditing')) {
      this.set('tempValue', undefined);
      this.set('isEditing', true);
      // TODO: find a better way to select only this input
      if (this.childViews[0].childViews.length > 0) {
        this.$(`#input-${this.childViews[0].childViews[0].elementId}`).focus();
      }
    }
  },

  actions: {
    change(value) {
      this.set('tempValue', value);
    },
    save() {
      let value = this.get('tempValue');

      // call parent action
      let _save = this.get('onSave');
      if (_save !== undefined) {
        return _save(value).then((response) => {
          if (response) return this.set('isEdit', false);
        });
      }

      let model = this.get('model');
      let attribute = this.get('attribute');

      // ignore if value isn't changed
      if ((model.get(attribute) == value) || (((model.get(attribute) == undefined) || (model.get(attribute) == null)) && value == "")) {
        return this.set('isEdit', false);
      }

      // set/save new value
      let oldValue = model.get(attribute);
      model.set(attribute, value);
      model.save().then(() => {
        this.set('isEdit', false);
      }).catch((error) => {
        model.set(attribute, oldValue);
        this.get('toastHandler').error(error);
      });
    },
    edit() {
      this.set('isEdit', true);
    },
    cancel() {
      let attribute = this.get(`model.${this.get('attribute')}`);
      let value = this.get('value');
      let empty = ((attribute === undefined) || (attribute === null));
      if ((empty && value != '') || (!empty && (attribute !== value))) {
        if (!confirm('Cancelar edição?')) return;
      }

      this.set('isEdit', false);
      this.set('isEditing', false);

      let model = this.get('model');
      if (model.get('isNew')) return this.set(`model.${this.get('attribute')}`, this.get('oldValue'));
      model.rollbackAttributes();
    },
    getCancel(event) {
      if (event.code == "Escape") {
        this.send("cancel");
      }
    }
  }
});
