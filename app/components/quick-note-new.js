import Component from '@ember/component';
import { inject as service } from '@ember/service';

export default Component.extend({
  store: service(),
  toastHandler: service(),

  note: undefined,
  model: undefined,

  init() {
    this._super(...arguments);
    this.set('note', this.get('store').createRecord('note'));
  },

  actions: {
    save() {
      let notes = this.get('model.notes'),
          note = this.get('note');

      notes.addObject(note);
      note.save()
        .catch((error) => {
          this.get('toastHandler').error(error);
        })
        .then(() => {
          this.set('note', this.get('store').createRecord('note'));
        });
    },
    cancel() {
      this.get('note').deleteRecord();
      this.set('note', this.get('store').createRecord('note'));
    },
    close() {}
  }
});
