import Component from '@ember/component';
import { computed } from '@ember/object';

export default Component.extend({
  model: undefined,
  attributes: computed('model', function() {
    let model = this.get('model');
    if (model !== undefined) {
      return model.formAttributes();
    }
    return [];
  }),

  actions: {
    save() {
      let model = this.get('model');
      model.save(); // .then(() => { });
    },
  }
});
