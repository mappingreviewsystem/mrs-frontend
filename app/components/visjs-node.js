// import Component from '@ember/component';
import { observer } from '@ember/object';
import VisjsNodeComponent from 'ember-cli-visjs/components/visjs-node';

export default VisjsNodeComponent.extend({
  /**
 * @public
 *
 * If set this gives the node is hidden.
 * @type {Boolean}
 */
  hidden: undefined,

  hiddenChanged: observer('hidden', function() {
    let container = this.get('containerLayer');
    container.updateNodeLevel(this.get('nId'), this.get('hidden'));
  })
});
