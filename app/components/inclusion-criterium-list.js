import Component from '@ember/component';
import { sort } from '@ember/object/computed';
import EmberObject from '@ember/object';

export default Component.extend({
  attributesMaker: undefined,

  sortFields: Object.freeze(['isLoaded:desc','title']),
  sortedCriteria: sort('inclusionCriteria', 'sortFields'),

  init() {
    this._super(...arguments);
    this.set('attributesMaker', ['title']);
  }
});
