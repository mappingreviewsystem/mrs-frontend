import Component from '@ember/component';
import { inject as service } from '@ember/service';
import { computed } from '@ember/object';
import { task, timeout } from 'ember-concurrency';
import { getNames } from 'ember-i18n-iso-countries';

export default Component.extend({
  store: service(),

  tagName: '',

  model: undefined,
  affiliation: undefined,
  newAffiliation: undefined,
  newLocation: undefined,

  countries: getNames('pt'),
  countryCodes: computed('countries', function() {
    let countries = [], countriesList = this.get('countries');
    for (var code in countriesList) {
      if (!countriesList.hasOwnProperty(code)) continue;
      let name = countriesList[code];
      countries.pushObject(Object.freeze({ code, name }));
    }
    return countries;
  }),

  init() {
    this._super(...arguments);

    this.set('newAffiliation', this.get('store').createRecord('affiliation'));
  },

  didReceiveAttrs() {
    this._super(...arguments);

    let isNew = (this.get('affiliation') === undefined);
    this.set('isNew', isNew);
    if (!isNew) {
      let location = this.get('affiliation.location');
      var newLocation = this.get('countryCodes').find((country) => { return country.code === location; });
      this.set('newLocation', newLocation);
    }
  },

  willDestroyElement() {
    this._super(...arguments);
    this.get('newAffiliation').deleteRecord();
  },

  searchTask: task(function* (term) {
    yield timeout(350);
    return yield this.get('store').query('affiliation', { filter: { name: term } }).then((affiliations) => {
      return affiliations;
    });
  }),

  actions: {
    save() {
      let affiliation = this.get('affiliation');
      let location = this.get('newLocation');
      this.set('affiliation.location', this.get('newLocation.code'));
      affiliation.save();

      this.get('newLocation',undefined);
      this.set('isEdit', false);
    },
    saveAssociation() {
      let model = this.get('model');
      let affiliation = this.get('affiliation');
      this.get('model.affiliations').pushObject(affiliation);
      this.set('affiliation', undefined);
      this.set('isEdit', false);

      this.get('model').save();
    },
    saveNew() {
      this.set('newAffiliation.location', this.get('newLocation.code'));
      let affiliation = this.get('newAffiliation');

      this.set('isEdit', false);
      this.set('newAffiliation', this.get('store').createRecord('affiliation'));

      this.get('model.affiliations').pushObject(affiliation);
      affiliation.save();
    },
    cancel() {
      let affiliation = this.get('affiliation');
      if (affiliation !== undefined) {
        affiliation.rollbackAttributes();
      }
      this.set('isEdit', false);
    },
    edit() {
      if (!this.get('affiliation.isNew')) {
        this.set('newLocation')
      }
      this.set('isEdit', true);
    },
    remove() {
      let modelName = this.get('model.name');
      let affiliation = this.get('affiliation');
      if (confirm(`Remover vinculação '${affiliation.get('name')}' de '${modelName}'?`)) {
        this.get('model.affiliations').removeObject(affiliation);
        this.get('model').save();
      }
    }
  }
});
