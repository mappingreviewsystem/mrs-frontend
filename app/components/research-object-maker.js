import Component from '@ember/component';
import { inject as service } from '@ember/service';
import { alias } from '@ember/object/computed';

export default Component.extend({
  store: service(),
  currentUser: service(),
  toastHandler: service(),

  /* expected variables */
  modelName: undefined,
  attributes: undefined,
  defaultValues: undefined,

  record: undefined,
  isMaker: false,

  currentResearch: alias('currentUser.currentResearch.content'),

  didReceiveAttrs() {
    this._super(...arguments);
    if (this.get('defaultValues') == undefined) this.set('defaultValues', {});
  },

  willDestroyElement() {
    if (this.get('record') !== undefined) {
      this.get('record').rollbackAttributes();
    }
  },

  actions: {
    openMaker() {
      let model = this.get('modelName');
      if (model !== undefined) {
        this.set('record', this.get('store').createRecord(model, this.get('defaultValues')));
        this.set('isMaker', true);
      }
    },

    closeMaker() {
      this.set('isMaker', false);
      this.set('record');
    },

    saveRecord() {
      let record = this.get('record');
      let research = this.get('currentResearch');
      this.send('closeMaker');

      record.set('research', research);
      record.save()
      .catch((error) => {
        record.set('research');
        this.get('toastHandler').error(error);
        this.send('openMaker');
        this.set('record', record);
      });
    }
  }
});
