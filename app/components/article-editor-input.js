import Component from '@ember/component';
import { inject as service } from '@ember/service';
import { computed } from '@ember/object';

export default Component.extend({
  i18n: service(),

  model: undefined,
  attribute: undefined,
  // value: computed('model.[]', 'attribute', function() {
  //   if (this.get('attribute') === 'contentType') {
  //     this.get('model.contentType').then((value) => { return value.get('code') });
  //   }
  //   else {
  //     let value = this.get(`model.${this.get('attribute')}`);
  //     if (value === undefined) {
  //       return this.get('i18n').t("input.blank");
  //     }
  //     return value;
  //   }
  // }),
  label: computed('model', 'attribute', function() {
    return this.get('i18n').t('article.' + this.get('attribute'));
  }),

  init() {
    this._super(...arguments);

    let key = `model.${this.get('attribute')}`;
    this.set('value', computed(key, function(){
      if (this.get('attribute') === 'contentType') {
        this.get('model.contentType.code');
        // this.get('model.contentType').then((value) => { return value.get('code') });
      }
      return this.get(`model.${this.get('attribute')}`);
    }));
  },

});
