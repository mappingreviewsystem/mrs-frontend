import Component from '@ember/component';
import { computed } from '@ember/object';
import { A } from '@ember/array';

export default Component.extend({
  classNames: ['flex'],

  label: undefined,
  value: undefined,
  type: undefined,
  disabled: false,

  // chips/select extra variables
  search: undefined,
  match: false,
  values: A([]),
  remaining: computed('value.length', function() {
    return this.get('values').filter((source) => {
      return source !== this.get('value.content');
    });
  }),

  // TODO: refactor please
  isType(type) {
    let _type = this.get('type');
    if (_type === undefined) return false;
    return _type == type;
  },
  isText: computed('type', function() {
    return this.isType('text');
  }),
  isText: computed('type', function() {
    return this.isType('number');
  }),
  isTextArea: computed('type', function() {
    return this.isType('textarea');
  }),
  isCheckbox: computed('type', function() {
    return this.isType('checkbox');
  }),
  isChips: computed('type', function() {
    return this.isType('chips');
  }),
  isPell: computed('type', function() {
    return this.isType('pell');
  }),
  isSelect: computed('type', function() {
    return this.isType('select');
  }),

  changeValue(value) {
    if (this.get('onChange') == undefined) return;
    this.get('onChange')(value);
  },

  actions: {
    change(value) {
      this.changeValue(value);
    },
    keydown(event) {
      if (this.get('onKeydown') == undefined) return;
      this.get('onKeydown')(event);
    },

    // chips input actions
    addContent(item) {
      let value = this.get('value');
      value.pushObject(item);

      this.changeValue(value);
    },
    removeContent(item) {
      let value = this.get('value');
      value.removeObject(item);

      this.changeValue(value);
    }
  }
});
