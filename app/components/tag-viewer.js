import Component from '@ember/component';
import { computed } from '@ember/object';
import { alias, sort } from '@ember/object/computed';
import { inject as service } from '@ember/service';

export default Component.extend({
  currentUser: service(),
  research: alias('currentUser.currentResearch'),

  model: undefined,
  addTag: true,

  isEditor: false,
  tag: undefined,

  tagsSorting: Object.freeze(['isLoaded:desc','label']),
  modelTags: sort('model.tags', 'tagsSorting'),
  tags: sort('research.tags', 'tagsSorting'),
  isTagsLoading: alias('research.tags.isPending'),

  actions: {
    openEditor(tag) {
      this.set('isEditor', true);
      this.set('tag', tag);
    },

    closeEditor() {
      this.set('isEditor', false);
    },

    change(tag) {
      let tags = this.get('model.tags');
      (tags.includes(tag)) ? tags.removeObject(tag) : tags.addObject(tag);

      this.get('model').save();
    }
  }
});
