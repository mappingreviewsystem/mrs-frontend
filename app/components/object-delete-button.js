import Component from '@ember/component';
import { inject as service } from '@ember/service';

export default Component.extend({
  store: service(),
  toastHandler: service(),

  model: undefined,
  message: undefined,
  promptResponse: undefined,

  actions: {
    delete() {
      let model = this.get('model');
      let message = this.get('message');

      if (!message) message = `Deseja deletar o ${model.constructor.modelName} #${model.id}?`;

      let promptResponse = this.get('promptResponse');
      let destroy = (promptResponse !== undefined) ? (prompt(message) == promptResponse) : confirm(message);

      if (destroy) {
        model.destroyRecord()
        .catch((error) => { this.get('toastHandler').error(error); });
      }
    }
  }
});
