import Component from '@ember/component';
import { inject as service } from '@ember/service';
import { computed } from '@ember/object';
import { alias, sort } from '@ember/object/computed';

export default Component.extend({
  store: service(),
  toastHandler: service(),

  article: undefined,
  type: undefined,
  research: alias('article.research'),
  sortFields: Object.freeze(['title']),
  sortedCriteria: sort('researchCriteria', 'sortFields'),
  criterium: computed('type', function() {
    return `${this.get('type')}Criterium`;
  }),

  initialized: false,


  didReceiveAttrs() {
    this._super(...arguments);

    if (!this.get('initialized')) {
      this.set('initialized', true);

      let type = this.get('type');

      let researchCriteria = `research.${type}Criteria`;
      this.set('researchCriteria', alias(researchCriteria));
      this.set('noCriteria', computed(researchCriteria, function() {
        return !this.get(researchCriteria).get('length');
      }));

      let typeUpper = type.charAt(0).toUpperCase() + type.slice(1);
      let articleCriteria = `article.article${typeUpper}Criteria`;
      this.set('articleCriteria', alias(articleCriteria));

      this.set('model', computed('researchCriteria.isPending', 'researchCriteria.@each', 'articleCriteria.isPending', 'articleCriteria.@each', function() {
        if (this.get('researchCriteria.isPending')) return [];

        let researchCriteria = this.get('sortedCriteria'),
            articleCriteria = this.get('articleCriteria'),
            type = this.get('type'),
            element = undefined, response = [];

        researchCriteria.forEach((criterium) => {
          element = articleCriteria.find((item) => {
            return item.belongsTo(`${type}Criterium`).id() == criterium.id
          });
          response.push({ researchCriterium: criterium, articleCriterium: element });
        });

        return response;
      }));
    }
  },

  actions: {
    change(researchCriterium, articleCriterium) {
      if (articleCriterium === undefined) {
        let type = this.get('type');
        let newCriterium = { article: this.get('article') };
        newCriterium[`${type}Criterium`] = researchCriterium;

        let model = this.get('store').createRecord(`article-${type}-criterium`, newCriterium);
        model.save().catch((error) => {
          model.deleteRecord();
          this.get('toastHandler').error(error);
        });
      }
      else {
        articleCriterium.destroyRecord().catch((error) => {
          this.get('toastHandler').error(error);
        });
      }
    }
  }
});
