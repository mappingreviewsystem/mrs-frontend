import Component from '@ember/component';
import { inject as service } from '@ember/service';
import { computed } from '@ember/object';
import { task, timeout, all } from 'ember-concurrency';

export default Component.extend({
  store: service(),
  toastHandler: service(),

  model: undefined,

  keywords: computed('model.keywords.isPending', function() {
    let keywords = this.get('model.keywords');
    if (keywords.get('length') > 0) return keywords.mapBy('title');
    return [];
  }),

  didReceiveAttrs() {
    this._super(...arguments);
    this.set('keyword', this.get('store').createRecord('keyword', {}));
    if (this.get('model.keywords.isPending')) this.get('model.keywords').reload();
  },

  willDestroyElement() {
    this._super(...arguments);
    this.get('keyword').deleteRecord();
  },

  createKeywords: task(function * (keywords) {
    if (keywords === undefined) keywords = [];

    let childTasks = [];
    keywords.forEach((title) => {
      childTasks.push(this.get('createKeyword').perform(title));
    });
    keywords = yield all(childTasks);

    yield this.set('model.keywords', []);
    yield this.get('model.keywords').pushObjects(keywords);

    return this.get('model').save()
    .then(() => { return true; })
    .catch((error) => { return this.get('errorTask').perform(error); });
  }).restartable(),

  createKeyword: task(function * (title) {
    let keyword = undefined;
    yield this.get('store').query('keyword', { filter: { title: title } })
    .then((k) => { keyword = k.get('firstObject'); })
    .catch((error) => { this.get('errorTask').perform(error); });

    if (keyword === undefined) {
      let keyword = this.get('store').createRecord('keyword', { title: title });
      yield keyword.save()
      .catch((error) => {
        keyword.deleteRecord();
        this.get('errorTask').perform(error);
      });
    }
    return keyword;
  }).enqueue().maxConcurrency(5),

  errorTask: task(function * (error) {
    yield this.get('createKeywords').cancelAll();

    this.get('toastHandler').error(error);
    yield timeout(4000);

    return false;
  }).drop(),

  actions: {
    save(keywords) {
      return this.get('createKeywords').perform(keywords);
    }
  }
});
