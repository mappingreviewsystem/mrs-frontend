import Component from '@ember/component';
import { sort } from '@ember/object/computed';

export default Component.extend({
  attributesMaker: Object.freeze(['key', 'value']),
  newValues: Object.freeze({ fieldType: "text", rangeMin: 0, rangeIncrement: 1 }),

  sortFields: Object.freeze(['isLoaded:desc','key']),
  sortedQuestions: sort('extractionQuestions', 'sortFields'),
});
