import Component from '@ember/component';
import { inject as service } from '@ember/service';
import $ from 'jquery';

export default Component.extend({
  store: service(),

  tagName: '',

  topbar: {isTopbar: true},
  content: {isContent: true},
  dialogActions: {isDialogActions: true},
  button: {isButton: true},

  showDialog: false,
  dialog: undefined,

  actions: {
    submit() {
      let _submit = this.get('onSubmit');
      if (_submit !== undefined) {
        _submit();
      }
    },
    open() {
      this.set('dialog', $(event.currentTarget));
      let _open = this.get('onOpen');
      if (_open !== undefined) {
        _open();
      }
      this.set('showDialog', true);
    },

    close() {
      let _close = this.get('onClose');
      if (_close !== undefined) {
        _close();
      }
      this.set('showDialog', false);
    },
  }
});
