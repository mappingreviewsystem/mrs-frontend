import Component from '@ember/component';
import { inject as service } from '@ember/service';
import { alias, sort } from '@ember/object/computed';

export default Component.extend({
  store: service(),
  toastHandler: service(),
  i18n: service(),

  extractionQuestion: undefined,
  isEditor: false,
  extractionQuestionOptions: alias('extractionQuestion.extractionQuestionOptions'),

  sortFields: Object.freeze(['title']),
  sortedOptions: sort('extractionQuestionOptions', 'sortFields'),

  actions: {
    editor() {
      let value = this.get('isEditor');
      this.set('isEditor', !value);
    },
    save() {
      this.get('extractionQuestion').save().then(() => {
        this.get('extractionQuestionOptions').forEach((option) => {
          if (option.get('hasDirtyAttributes')) option.save();
        });
        this.set('isEditor', false);
      }).catch((error) => {
        this.get('toastHandler').error(error);
      });
    },
    cancel() {
      this.get('extractionQuestion').rollbackAttributes();
      this.set('isEditor', false);
    },

    new() {
      let option = this.get('store').createRecord('extractionQuestionOption', {
        title: this.get('i18n').t('extraction-question-option.newRecord'),
        extractionQuestion: this.get('extractionQuestion')
      });

      option.save()
      .catch((error) => {
        option.destroyRecord();
        this.get('toastHandler').error(error);
      });
    }
  }
});
