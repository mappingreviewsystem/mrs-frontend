import Component from '@ember/component';
import { inject as service } from '@ember/service';

export default Component.extend({
  store: service(),

  object: null,
  model: undefined,
  attributes: undefined,

  didReceiveAttrs() {
    this._super(...arguments);
    this.reset();
  },

  reset() {
    let model = this.get('model');
    let object = this.get('store').createRecord(model);
    this.set('object', object);
  },

  actions: {
    save() {
      let object = this.get('object');
      if (object !== null) {
        object.save().then(() => { this.reset() });
      }
    }
  }
});
