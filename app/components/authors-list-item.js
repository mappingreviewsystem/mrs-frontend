import Component from '@ember/component';

export default Component.extend({
  tagName: '',

  model: undefined,
  author: undefined,

  actions: {
    remove() {
      this.get('model.authors').removeObject(this.get('author'));
      this.get('model').save();
    }
  }
});
