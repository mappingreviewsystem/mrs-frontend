import Component from '@ember/component';
import { inject as service } from '@ember/service';
import { computed } from '@ember/object';
import { task, timeout } from 'ember-concurrency';

export default Component.extend({
  i18n: service(),
  store: service(),

  model: undefined,
  referenceType: undefined,
  isNew: false,
  newReference: undefined,

  references: computed('model', 'referenceType', function() {
    return this.get(`model.${this.get('referenceType')}`);
  }),

  referenceDash: computed('referenceType', function() {
    return this.get('referenceType').dasherize();
  }),

  searchTask: task(function* (term) {
    yield timeout(350);
    return yield this.get('store').query('article', { search: term }).then((articles) => {
      return articles;
    });
  }),

  actions: {
    new() {
      this.set('isNew', true);
    },
    cancel() {
      this.set('isNew', false);
      this.set('newReference');
    },
    save() {
      let reference = this.get('newReference');
      if (reference === undefined) return alert(this.get('i18n').t('article-reference.blankRecord'));

      this.set('isNew', false);
      this.set('newReference');

      let model = this.get('model');
      model.get(this.get('referenceType')).pushObject(reference);
      model.save();
    },
    remove(reference) {
      if (!confirm(this.get('i18n').t('article-reference.destroyRecordQuestion'))) return;

      let model = this.get('model');
      model.get(this.get('referenceType')).removeObject(reference);
      model.save();
    }
  }
});
