import Component from '@ember/component';
import { inject as service } from '@ember/service';

export default Component.extend({
  currentUser: service(),
  toastHandler: service(),

  research: undefined,
  groups: undefined,

  actions: {
    changeGroup(group) {
      let research = this.get('research');
      let oldGroup = this.get('research.group');

      research.set('group', group);
      research.save()
        .catch((error) => {
          research.set('group', oldGroup);
          this.get('toastHandler').error(error);
        });
    }
  }
});
