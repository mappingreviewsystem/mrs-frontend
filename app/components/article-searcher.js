import Component from '@ember/component';
import { inject as service } from '@ember/service';
import { computed } from '@ember/object';
import { alias, sort } from '@ember/object/computed';
import { isBlank } from '@ember/utils';

export default Component.extend({
  router: service(),
  currentUser: service(),
  i18n: service(),

  search: '',
  articlesCount: undefined,
  selectedFilters: undefined,

  didReceiveAttrs() {
    this._super(...arguments);
    // preload filters
    if (this.get('research.articleDatabases.isPending')) this.get('research').hasMany('articleDatabases').reload();
    if (this.get('research.inclusionCriteria.isPending')) this.get('research').hasMany('inclusionCriteria').reload();
    if (this.get('research.exclusionCriteria.isPending')) this.get('research').hasMany('exclusionCriteria').reload();
    if (this.get('research.tags.isPending')) this.get('research').hasMany('tags').reload();
  },

  totalCount: computed('articlesCount', function() {
    let articlesCount = this.get('articlesCount');
    if (articlesCount.not_included === undefined) return articlesCount.total;
    return (articlesCount.total - articlesCount.not_included);
    // return `${articlesCount.total - articlesCount.not_included} + ${articlesCount.not_included} (${articlesCount.total})`;
  }),

  research: alias('currentUser.currentResearch.content'),
  tags: sort('research.tags', 'tagsSorting'),
  tagsSorting: Object.freeze(['label']),
  articleDatabases: sort('research.articleDatabases', 'titleSorting'),
  inclusionCriteria: sort('research.inclusionCriteria', 'titleSorting'),
  exclusionCriteria: sort('research.exclusionCriteria', 'titleSorting'),
  titleSorting: Object.freeze(['title']),

  filters: computed('research.{articleDatabases,inclusionCriteria,exclusionCriteria}.@each.title', 'research.tags.@each.{label,color}', function() {
    let filters = {
      status: [],
      tags: [],
      articleDatabases: [],
      inclusionCriteria: [],
      exclusionCriteria: []
    };
    let i18n = this.get('i18n');

    // status
    filters['status'].push(
      { type: 'status', value: 'extracted', label: i18n.t('article.status.extracted') },
      { type: 'status', value: 'selected', label: i18n.t('article.status.selected') },
      { type: 'status', value: 'unseen', label: i18n.t('article.status.unseen') },
      { type: 'status', value: 'excluded', label: i18n.t('article.status.excluded') },
      { type: 'status', value: 'not_included', label: i18n.t('article.status.not_included') },
    );
    // tags
    this.get('tags').forEach((element) => {
      filters['tags'].push({ type: 'tags', value: element.id, label: element.get('label'), color: element.get('color') });
    });
    // articleDatabases
    // filters['articleDatabases'].push({ type: 'articleDatabases', value: null, label: i18n.t('search-filters.articleDatabases.none') });
    this.get('articleDatabases').forEach((element) => {
      filters['articleDatabases'].push({ type: 'articleDatabases', value: element.id, label: element.get('title') });
    });
    // criteria
    this.get('inclusionCriteria').forEach((element) => {
      filters['inclusionCriteria'].push({ type: 'inclusionCriteria', value: element.id, label: element.get('title') });
    });
    this.get('exclusionCriteria').forEach((element) => {
      filters['exclusionCriteria'].push({ type: 'exclusionCriteria', value: element.id, label: element.get('title') });
    });

    return filters;
  }),

  chipsFilters: computed('filters.{tags,status,articleDatabases,inclusionCriteria,exclusionCriteria}', function() {
    let filters = this.get('filters');
    if (isBlank(filters)) return [];

    let array = new Array();
    for (var key in filters) {
      array.pushObjects(filters[key]);
    }
    return array;
  }),
  chipsSelectedFilters: computed('selectedFilters', 'filters', function() {
    let chips = this.get('selectedFilters');
    let filters = this.get('filters');
    if (isBlank(chips) || isBlank(filters)) return [];

    let array = new Array();
    let icons = { 'exclusionCriteria': 'square-o', 'inclusionCriteria': 'check-square-o', 'status': 'globe', 'tags': 'tag' };

    for (var key in chips) {
      if (isBlank(chips[key])) continue;
      for (var value in chips[key]) {
        if (!chips[key].hasOwnProperty(value)) continue;
        let obj = filters[key].find(v => { return v.value == chips[key][value]; });

        if (obj === undefined) continue;
        array.push({ type: key, label: obj['label'], value: obj['value'], icon: icons[key] });
      }
    }
    return array;
  }),

  actions: {
    change(item) {
      this.get('changeFilter')(item);
    }
  }
});
