import Component from '@ember/component';
import { inject as service } from '@ember/service';
import { computed } from '@ember/object';

export default Component.extend({
  store: service(),
  fileUpload: service(),

  model: undefined,

  isNew: computed('model.status', function() {
    let status = this.get('model.status');
    if (!status) return true;
    return status === "not_imported";
  }),

  isImported: computed('model.status', function() {
    let status = this.get('model.status');
    if (!status) return false;
    return status === "imported";
  }),

  filename: computed('model.exportedSearch', function() {
    let exportedSearch = this.get('model.exportedSearch');
    if ((exportedSearch === "") || !exportedSearch) return "";
    return exportedSearch.split('/').pop().split("?")[0]
  }),

  actions: {
    uploadExportedSearch(file) {
      let fileKey = "data[attributes][exported_search]";
      let url = `/article_databases/${this.get('model.id')}`;
      this.get('fileUpload').upload(file, fileKey, url, "PATCH").then((response) => {
        this.get('store').pushPayload(response.body);
      });
    },

    import() {
      this.get('model').import({ download: true }).then((response) => {
        this.get('store').pushPayload(response);
      })
    }
  }
});
