import Component from '@ember/component';
import { inject as service } from '@ember/service';
import { alias } from '@ember/object/computed';

export default Component.extend({
  store: service(),
  currentUser: service(),

  research: alias('currentUser.currentResearch'),

  model: undefined,
  tag: undefined,
  close: undefined,
  colors: ["B71C1C", "880E4F", "4A148C", "311B92", "1A237E", "0D47A1", "01579B", "006064", "004D40", "1B5E20", "33691E", "827717", "E65100", "BF360C", "4E342E", "424242", "37474F", "000000"],

  didReceiveAttrs() {
    this._super(...arguments);

    if ((this.get('tag') === undefined) && (this.get('model') !== undefined)) {
      let tag = this.get('store').createRecord('tag', {
          label: '',
          color: '000000',
      });
      this.set('tag', tag);
    }
  },

  willDestroyElement() {
    this._super(...arguments);

    this.get('tag').rollbackAttributes();

    if (this.get('close') !== undefined) {
      this.get('close')();
    }
  },

  actions: {
    save() {
      this.set('tag.research', this.get('research'));
      let tag = this.get('tag');
      this.get('model.tags').addObject(tag);
      tag.save();

      if (this.get('close') !== undefined) {
        this.get('close')();
      }
    },

    delete() {
      let tag = this.get('tag');
      tag.deleteRecord();
      tag.save();

      if (this.get('close') !== undefined) {
        this.get('close')();
      }
    }
  }
});
