// import Component from '@ember/component';
import VisjsNetworkComponent from 'ember-cli-visjs/components/visjs-network';
import { inject as service } from '@ember/service';

export default VisjsNetworkComponent.extend({
  store: service(),

  popupArticle: undefined,

  didInsertElement() {
    this._super(...arguments);

    let network = this.get('network');
    let _this = this.get('storeAs');

    network.off('selectNode');
    network.on('selectNode', (e) => {
      let [ selectedNode ] = e.nodes;

      let nodes = _this.nodes;
      let store = this.get('store');

      // store.findRecord('article', selectedNode).then(function(article) {
      // });

      // store.findRecord('article', selectedNode).then(function(article) {
      //   article.referenceGraphs().then(function(response) {
      //     store.push(response.nodes);
      //     store.push(response.edges);
      //
      //     response.nodes.data.forEach(function(node) {
      //       node = node.attributes;
      //       nodes.update({id: node.article.id, label: node.label, color: node.color,
      //         shape: node.shape, value: node.value, hidden: false,
      //         relevant: node.relevant })
      //     });
      //     network.moveTo(selectedNode);
      //   });
      // });
    });

    network.on('deselectNode', (e) => {
      let [ selectedNode ] = e.previousSelection.nodes;

      // let children = network.getConnectedNodes(selectedNode, "to");
      // let nodes = _this.nodes;
      // let child = undefined;
      // children.forEach(function(element) {
      //   child = nodes.get(element);
      //   if (!child.relevant) {
      //     nodes.remove(child.id);
      //   }
      // });
    });

    network.on("startStabilizing", function () {
      network.setOptions( { physics: true } );
    });
    network.on("stabilizationIterationsDone", function () {
      network.setOptions( { physics: false } );
    });
  },

  willDestroyElement() {
    this._super(...arguments);

    // destroy event listeners
    let network = this.get('network');
    network.off('selectNode');
    network.off('selectEdge');
    network.off('dragEnd');
    network.off('beforeDrawing');
    network.off('deselectNode');
    network.off('startStabilizing');
    network.off('stabilizationIterationsDone');
  },

  addNode(node) {
    this._super(...arguments);

    let nodes = this.get('nodes');
    let simplifiedNode = nodes.get(node.get('nId'));

    if (node.get('hidden')) {
      simplifiedNode.hidden = node.get('hidden');
    }

    if (node.get('relevant')) {
      simplifiedNode.relevant = node.get('relevant');
    }

    nodes.update(simplifiedNode, simplifiedNode.id);
  },

  updateNodeHidden(nId, hidden) {
    this.get('nodes').update({ id: nId, hidden });
  }
});
