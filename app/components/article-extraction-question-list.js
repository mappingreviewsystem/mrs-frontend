import Component from '@ember/component';
import { inject as service } from '@ember/service';
import { computed } from '@ember/object';
import { alias, sort } from '@ember/object/computed';

export default Component.extend({
  store: service(),

  article: undefined,
  research: alias('article.research'),
  sortFields: Object.freeze(['key']),
  sortedQuestions: sort('research.extractionQuestions', 'sortFields'),
  noQuestion: computed('research.extractionQuestions', function() {
    return !this.get('research.extractionQuestions').get('length');
  }),

  model: computed('research.extractionQuestions.isPending', 'research.extractionQuestions.[]', 'article.articleExtractionQuestions.isPending', 'article.articleExtractionQuestions.[]', function() {
    if (this.get('research.extractionQuestions.isPending')) return [];

    let researchQuestions = this.get('sortedQuestions'),
        articleQuestions = this.get('article.articleExtractionQuestions'),
        element = undefined, response = [];

    researchQuestions.forEach((question) => {
      element = articleQuestions.find((item) => {
        return (!item.get('isDeleted') && (item.belongsTo('extractionQuestion').id() == question.id))});
      response.push({ researchQuestion: question, articleQuestion: element });
    });

    return response;
  }),

  initialized: false,

  didReceiveAttrs() {
    this._super(...arguments);

    if (!this.get('initialized')) {
      this.set('initialized', true);

      // create missing article-criteria for object-input-editor
      if (this.get('article') !== undefined) {
        this.get('research.extractionQuestions').then((questions) => {
          this.get('article.articleExtractionQuestions').then((articleQuestions) => {
            let element = undefined;
            let newQuestion = {
              article: this.get('article'),
              value: ''
            };
            questions.forEach((question) => {
              element = articleQuestions.find((item) => item.belongsTo('extractionQuestion').id() == question.id);
              if (element == undefined) {
                newQuestion.extractionQuestion = question;
                this.get('store').createRecord('article-extraction-question', newQuestion);
              }
            });
          });
        });
      }
    }
  },
});
