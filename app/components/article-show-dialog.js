import Component from '@ember/component';
import { inject as service } from '@ember/service';

export default Component.extend({
  store: service(),
  i18n: service(),

  article: undefined,

  isImporting: false,

  actions: {
    import(references) {
      this.set('isImporting', true);

      let message = (references) ? 'article.import-metadata-references-confirmation' : 'article.import-metadata-confirmation';
      if (confirm(this.get('i18n').t(message))) {
        return this.get('article').crossref({ references }).then(response => {
          this.get('store').pushPayload('article', response);
          this.set('isImporting', false);
        });
      }

      this.set('isImporting', false);
    },
    include() {
      let article = this.get('article');
      if (article.get('status') === 'not_included') {
        article.set('status', 'unseen');
        article.save();
      }
      else if (article.get('articleDatabase.content') === null) {
        article.set('status', 'not_included');
        article.save();
      }
    }
  }
});
