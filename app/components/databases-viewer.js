import Component from '@ember/component';
import { inject as service } from '@ember/service';
import { computed } from '@ember/object';
import { alias } from '@ember/object/computed';

export default Component.extend({
  store: service(),

  research: undefined,

  articleDatabases: alias('research.articleDatabases'),

  actions: {
    createBase() {
      this.get('store').createRecord('article-database', { research: this.get('research') });
    },
    removeBase(articleDatabase) {
      articleDatabase.destroyRecord();
    }
  }
});
