import Component from '@ember/component';
import { inject as service } from '@ember/service';
import { task, timeout } from 'ember-concurrency';
import { sort } from '@ember/object/computed';

export default Component.extend({
  store: service(),
  currentUser: service(),
  i18n: service(),
  toastHandler: service(),

  group: undefined,
  membershipsSorting: Object.freeze(['owner', 'user.name']),
  memberships: sort('group.groupMemberships', 'membershipsSorting'),
  isNew: false,
  newMember: undefined,
  memberTypes: undefined,
  newMemberType: undefined,

  init() {
    this._super(...arguments);
    let i18n = this.get("i18n");
    let memberTypes = Object.freeze([
      { type: 'owner', name: i18n.t('group-membership.owner') },
      { type: 'member', name: i18n.t('group-membership.member') }
    ]);
    this.set('memberTypes', memberTypes);
    this.set('newMemberType', memberTypes[1]);
  },

  searchTask: task(function* (term) {
    yield timeout(350);
    return yield this.get('store').query('user', { filter: { name: term } }).then(authors => { return authors; });
  }),

  actions: {
    new() {
      this.set('isNew', true);
    },
    remove(membership) {
      if (confirm(this.get('i18n').t('group-membership.destroyRecordQuestion'))) membership.destroyRecord();
    },
    save() {
      let member = this.get('newMember');
      if (member === undefined) return alert(this.get('i18n').t('group-membership.chooseMember'));

      let group = this.get('group');
      let type = this.get('newMemberType');
      let membership = this.get('store').createRecord('group-membership', { user: member, group, owner: (type === "owner") });
      membership.save().then(() => {
        this.set('isNew', false);
        this.set('newMember');
        this.set('newMemberType', 'member');
      }).catch((error) => {
        membership.deleteRecord();
        this.get('toastHandler').error(error);
      });
    },
    cancel() {
      this.set('newMember');
      this.set('isNew', false);
    }
  }
});
