import Component from '@ember/component';
import { inject as service } from '@ember/service';

export default Component.extend({
  notesSidenav: service(),

  article: undefined,

  // buttons
  show: true,
  extract: true,
  notes: true,
  url: true,
  pdf: true,
  delete: true,

  actions: {
    toggleSidenav(article) {
      if (article.get('notes.isPending')) article.reload();
      this.get('notesSidenav').toggle(article);
    },
  }
});
