import Component from '@ember/component';
import { inject as service } from '@ember/service';
import { computed } from '@ember/object';
import { alias } from '@ember/object/computed';
import { task, timeout } from 'ember-concurrency';

export default Component.extend({
  store: service(),
  toastHandler: service(),

  article: undefined,
  extractionQuestion: undefined,
  articleExtractionQuestion: undefined,
  timerValue: undefined,

  extractionQuestionOptions: alias('extractionQuestion.extractionQuestionOptions'),
  articleExtractionQuestionOptions: computed('extractionQuestionOptions.isPending', 'articleExtractionQuestion.isPending', 'articleExtractionQuestion.extractionQuestionOptions.isPending', 'articleExtractionQuestion.extractionQuestionOptions.[]', function() {
    let articleOptions = this.get('articleExtractionQuestion.extractionQuestionOptions');
    let options = this.get('extractionQuestionOptions');
    if (articleOptions == undefined || options == undefined) return [];
    if (articleOptions.get('isPending') || options.get('isPending')) return [];

    let ids = this.get('articleExtractionQuestion').hasMany('extractionQuestionOptions').ids();
    return options.map((option) => {
      return { option: option, checked: ids.includes(option.id) };
    });
  }),

  rangeArray: computed('extractionQuestion.rangeMin', 'extractionQuestion.rangeMax', 'extractionQuestion.rangeIncrement', function() {
    let min = this.get('extractionQuestion.rangeMin');
    let max = this.get('extractionQuestion.rangeMax');
    let increment = this.get('extractionQuestion.rangeIncrement');
    let n = Math.ceil((max - min) / increment);
    return Array.apply(null, {length: n}).map((_v, index) => (min + (increment * index)).toString())
  }),

  saveAction: function(value, promise) {
    if (value === '' || value === null) {
      let aeq = this.get('articleExtractionQuestion');
      aeq.destroyRecord().then(() => {
        let article = this.get('article');
        let newQuestion = {
          article: article,
          value: '',
          extractionQuestion: this.get('extractionQuestion')
        };
        this.get('store').createRecord('article-extraction-question', newQuestion);
      })
      .catch((error) => {
        aeq.rollbackAttributes();
        this.get('toastHandler').error(error);
      });
      if (promise) return new Promise(() => true);
      return;
    }

    let articleExtractionQuestion = this.get('articleExtractionQuestion');
    articleExtractionQuestion.set('value', value);
    return articleExtractionQuestion.save().then(() => { return true; })
    .catch((error) => {
      articleExtractionQuestion.rollbackAttributes();
      this.get('toastHandler').error(error);
      return false;
    });
  },

  saveSingleAction: function(option) {
    let articleExtractionQuestion = this.get('articleExtractionQuestion');
    articleExtractionQuestion.set('extractionQuestionOption', option);

    return articleExtractionQuestion.save()
    .catch((error) => {
      articleExtractionQuestion.rollbackAttributes();
      this.get('toastHandler').error(error);
    });
  },

  saveTimerTask: task(function * (value) {
    this.get('articleExtractionQuestion').set('value', value);
    yield timeout(1000);
    return this.saveAction(value);
  }).restartable(),

  actions: {
    save(value) {
      return this.saveAction(value, true);
    },

    saveTimer(value) {
      return this.saveTimerTask.perform(value);
    },

    saveSelectOption(option) {
      let value = (option == undefined) ? null : option;
      return this.saveSingleAction(option);
    },

    saveRadioOption(optionId) {
      let option = null;
      if (optionId !== null) option = this.get('extractionQuestionOptions').find((item) => item.id == optionId);
      if (option == undefined) option = null;

      return this.saveSingleAction(option);
    },

    saveMultiOption(option, value) {
      // TODO: fix not saving empty relationships AND initial value being blank now
      let articleExtractionQuestion = this.get('articleExtractionQuestion');
      return articleExtractionQuestion.get('extractionQuestionOptions').then((options) => {
        (value == false) ? options.removeObject(option) : options.addObject(option);

        return articleExtractionQuestion.save()
        .then((aeq) => {
          this.get('articleExtractionQuestion').reload();
        })
        .catch((error) => {
          (value == false) ? options.addObject(option) : options.removeObject(option);
          this.get('toastHandler').error(error);
        });
      });
    },
  }
});
