import Component from '@ember/component';

export default Component.extend({
  filters: undefined,
  locale: undefined,
  icon: undefined,
  selectedFilters: undefined,

  actions: {
    change(item) {
      this.get('changeItem')(item);
    }
  }
});
