import Component from '@ember/component';
import { inject as service } from '@ember/service';
import { isEmpty } from '@ember/utils';
import { alias } from '@ember/object/computed';

export default Component.extend({
  store: service(),
  currentUser: service(),

  tagName: '',

  articleDatabase: undefined,

  init() {
    this._super(...arguments);
    this.get('store').findAll('content-type').then((types) => {
      this.set('contentTypes', types);
    });
  },

  actions: {
    open() {
      this.set('article', this.get('store').createRecord('article', {
        articleDatabase: this.get('articleDatabase')
      }));
      this.set('isNew', true);
    },

    close() {
      if (this.get('isNew')) {
        this.get('article').deleteRecord();
      }
    },
    save(close) {
      this.get('article').save().then(() => {
        this.get('currentUser').reloadResearch();
        this.set('isNew', false);
        close();
      });
    }
  }
});
