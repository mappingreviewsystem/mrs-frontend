import Component from '@ember/component';
import { inject as service } from '@ember/service';
import { computed } from '@ember/object';

export default Component.extend({
  store: service(),
  currentUser: service(),
  notesSidenav: service(),

  open: undefined,
  isSidenavOpen: false,

  model: undefined,
  notes: computed('model.@each.notes', function() {
    return this.get('model.notes');
  }),

  label: computed('model', function() {
    let label = this.get('model.title');

    if (label === undefined) {
      return '';
    }
    else if (label.length > 30) {
      return label.substring(0,30) + '...';
    }

    return label;
  }),

  actions: {
    new() {
      let model = this.get('model');
      model.get('notes').then(() => {
        let note = this.get('store').createRecord('note', { value: "" });
        model.get('notes').addObject(note);
        note.save();
      });
    },
    close() {
      this.get('notesSidenav').close();
    }
  }
});
