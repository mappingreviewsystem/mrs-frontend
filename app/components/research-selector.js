import Component from '@ember/component';
import { inject as service } from '@ember/service';
import { computed } from '@ember/object';
import { alias } from '@ember/object/computed';
import getOwner from "ember-owner/get";

export default Component.extend({
  store: service(),
  router: service(),
  currentUser: service(),

  currentResearch: alias('currentUser.currentResearch.content'),
  researches: computed('currentUser.user.allResearches', 'currentResearch',  function() {
    return this.get('currentUser.user.allResearches').filter((research) => {
      return research !== this.get('currentResearch');
    });
  }),

  actions: {
    changeResearch: function(research) {
      let user = this.get('currentUser.user');
      user.set('currentResearch', research);
      user.save().then(() => {
        // refresh route to reflect new research
        let currentRouteName = this.get('router.currentRouteName');
        let currentRouteInstance = getOwner(this).lookup(`route:${currentRouteName}`);
        currentRouteInstance.refresh();
      });

    }
  }
});
