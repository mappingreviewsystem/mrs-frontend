import Component from '@ember/component';
import { inject as service } from '@ember/service';
// import { computed } from '@ember/object';
import { task, timeout } from 'ember-concurrency';

export default Component.extend({
  store: service(),

  isNew: false,
  newAuthor: undefined,
  selectedAuthor: undefined,
  model: undefined,

  init() {
    this._super(...arguments);

    this.set('newAuthor', this.get('store').createRecord('author'));
  },

  searchTask: task(function* (term) {
    yield timeout(350);
    return this.get('store').query('author', { filter: { name: term } }).then((authors) => { return authors; });
  }),

  willDestroyElement() {
    this._super(...arguments);
    this.resetAuthors();
  },

  resetAuthors() {
    this.get('newAuthor').deleteRecord();
    this.set('newAuthor', this.get('store').createRecord('author'));
    this.set('selectedAuthor', undefined);
  },

  actions: {
    new() {
      this.set('isNew', true);
    },
    save() {
      let selectedAuthor = this.get('selectedAuthor');
      let newAuthor = this.get('newAuthor');
      if (selectedAuthor !== undefined) {
        this.get('model.authors').pushObject(selectedAuthor);
        this.get('model').save();
      }
      else if (newAuthor.get('name') !== undefined) {
        this.get('model.authors').pushObject(newAuthor);
        newAuthor.save();
        this.set('newAuthor', this.get('store').createRecord('author'));
      }

      this.set('selectedAuthor', undefined);
      this.set('isNew', false);
    },
    cancel() {
      this.resetAuthors();
      this.set('isNew', false);
    }
  }
});
