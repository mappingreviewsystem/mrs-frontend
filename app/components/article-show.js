import Component from '@ember/component';
import { computed } from '@ember/object';
import { alias } from '@ember/object/computed';
import { inject as service } from '@ember/service';

export default Component.extend({
  store: service(),
  fileUpload: service(),

  article: undefined,
  loadArticle: true,

  publication: alias('article.publication.content'),

  keywordList: computed('article.keywords', function() {
    let list = this.get('article.keywords');
    if (list.get('length') < 1) return "";
    return list.mapBy('title').join('; ');
  }),

  didReceiveAttrs() {
    this._super(...arguments);

    let article = this.get('article');
    if (typeof article === "number") {
      this.set('article', this.get('store').find('article', article));
    }
    else if (this.get('loadArticle') === true) {
      article.reload();
    }
  },

  actions: {
    uploadFile(file) {
      let fileKey = "data[attributes][file]";
      let url = `/articles/${this.get('article.id')}`;
      this.get('fileUpload').upload(file, fileKey, url, "PATCH").then((response) => {
        this.get('store').pushPayload(response.body);
      });
    }
  }
});
