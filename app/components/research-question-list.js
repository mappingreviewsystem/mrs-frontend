import Component from '@ember/component';
import { sort } from '@ember/object/computed';
import EmberObject from '@ember/object';

export default Component.extend({
  attributesMaker: undefined,
  isGuide: false,
  isNew: true,

  sortFields: Object.freeze(['isLoaded:desc','title']),
  sortedQuestions: sort('researchQuestions', 'sortFields'),

  init() {
    this._super(...arguments);
    this.set('attributesMaker', ['title', 'description']);
  },

  actions: {
    toggle(researchQuestion) {
      this.get('toggleSidenav')(researchQuestion);
    }
  }
});
