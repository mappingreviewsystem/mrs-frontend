import Component from '@ember/component';
import { inject as service } from '@ember/service';
import { alias } from '@ember/object/computed';
import { task, timeout } from 'ember-concurrency';
import { later } from '@ember/runloop';

export default Component.extend({
  store: service(),

  model: undefined,
  selectedPublication: undefined,
  publication: alias('model.publication.content'),

  didReceiveAttrs() {
    this._super(...arguments);
    if (this.get('model.publication.content') === null) {
      let publication = this.get('store').createRecord('publication', { title: "Nova publicação" });
      later(() => {
        this.set('model.publication', publication);
      });
    }
    else if (this.get('publication.isPending')) this.get('publication').reload();
  },

  searchTask: task(function* (term) {
    yield timeout(350);
    return this.get('store').query('publication', { filter: { title: term } }).then((publications) => { return publications; });
  }),

  actions: {
    new() {
      this.set('isNew', true);
    },
    create() {
      if (!confirm("Criar uma nova publicação?")) return;

      let publication = this.get('store').createRecord('publication', { title: "Nova publicação2" });
      publication.get('articles').pushObject(this.get('model'));
    },
    save() {
      let publication = this.get('selectedPublication');
      if (publication === undefined) return alert('Selecione alguma publicação antes.');

      if (this.get('publication.isNew')) publication.save();
      this.set('model.publication', publication);
      this.get('model').save();

      this.set('selectedPublication', undefined);
      this.set('isNew', false);
    },
    cancel() {
      this.set('selectedPublication', undefined);
      this.set('isNew', false);
    }
  }
});
