import Component from '@ember/component';
import { inject as service } from '@ember/service';
import { computed } from '@ember/object';
import { alias, filter } from '@ember/object/computed';
import { task, timeout } from 'ember-concurrency';

export default Component.extend({
  notesSidenav: service(),

  researchQuestion: undefined,
  model: undefined,

  extractionQuestions: alias('researchQuestion.extractionQuestions'),

  didReceiveAttrs() {
    this._super(...arguments);

    if (this.get('researchQuestion') !== undefined) {
      this.get('createModel').perform();
    }
  },

  createModel: task(function*() {
    return this.get('extractionQuestions').then((extractionQuestions) => {
      if (extractionQuestions == undefined) return [];

      let response = {};
      extractionQuestions.forEach((extractionQuestion) => {
        let articleExtractionQuestions = extractionQuestion.get('articleExtractionQuestions')
        response[extractionQuestion.get('key')] = articleExtractionQuestions;
      });
      this.set('model', response);
    });
  }).restartable(),

  actions: {
    toggleSidenav: function(article) {
      this.get('notesSidenav').toggle(article);
    },
  }
});
