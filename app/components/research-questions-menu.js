import Component from '@ember/component';
import { alias, sort } from '@ember/object/computed';

export default Component.extend({
  extractionQuestion: undefined,
  research: alias('extractionQuestion.research'),
  researchQuestionsSorting: Object.freeze(['title']),
  researchQuestions: sort('research.researchQuestions', 'researchQuestionsSorting'),

  actions: {
    change(researchQuestion) {
      let researchQuestions = this.get('extractionQuestion.researchQuestions');
      (researchQuestions.includes(researchQuestion)) ? researchQuestions.removeObject(researchQuestion) : researchQuestions.addObject(researchQuestion);

      this.get('extractionQuestion').save();
    }
  }
});
