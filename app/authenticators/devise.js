import DeviseAuthenticator from 'ember-simple-auth/authenticators/devise';
import { Promise } from 'rsvp';
import { isEmpty } from '@ember/utils';
import { getProperties, get, computed } from '@ember/object';
import { run } from '@ember/runloop';
import { inject as service } from '@ember/service';
import { alias } from '@ember/object/computed';
import { task } from 'ember-concurrency';

export default DeviseAuthenticator.extend({
  config: service(),

  host: alias('config.api.host'),
  namespace: alias('config.api.namespace'),

  loginEndpoint: computed('config.api.host', 'config.api.namespace', function() {
    return `${this.get('config.api.host')}/${this.get('config.api.namespace')}/auth/sign_in`;
  }),
  logoutEndpoint: computed('config.api.host', 'config.api.namespace', function() {
    return `${this.get('config.api.host')}/${this.get('config.api.namespace')}/auth/sign_out`;
  }),

  identificationAttributeName: 'email',

  authenticate(identification, password) {
    return new Promise((resolve, reject) => {
      let { identificationAttributeName } = getProperties(this, 'identificationAttributeName');
      let data = { password };
      data[identificationAttributeName] = identification;

      let requestOptions = { url: get(this, 'loginEndpoint') };

      this.makeRequest(data, requestOptions).then((response) => {
        if (response.ok) {
          response.json().then((json) => {
            let data = {
              account: json['data'],
              accessToken: response.headers.get('access-token'),
              expiry: response.headers.get('expiry'),
              tokenType: response.headers.get('token-type'),
              uid: response.headers.get('uid'),
              client: response.headers.get('client')
            };

            if (this._validate(data)) {
              run(null, resolve, data);
            } else {
              run(null, reject, 'Check that server response header includes data token and valid.');
            }
          });
        } else {
          response.json().then((json) => run(null, reject, json));
        }
      }).catch((error) => run(null, reject, error));
    });
  },

  invalidate(data) {
    return this.get('_invalidateTask').perform(data);
  },

  _invalidateTask: task(function * (data) {
    return yield new Promise((resolve, reject) => {
      let headers = {
        'access-token': data.accessToken,
        'expiry': data.expiry,
        'token-type': data.tokenType,
        'uid': data.uid,
        'client': data.client
      };

      let requestOptions = {
        url: get(this, 'logoutEndpoint'),
        method: 'DELETE',
        headers
      };

      this.makeRequest({}, requestOptions).then((response) => {
        response.json().then((json) => {
          run(null, resolve, json);
        });
      }).catch((error) => run(null, reject, error));
    });
  }).drop(),

  _validate(data) {
    let now = (new Date()).getTime();

    return !isEmpty(data.accessToken) && !isEmpty(data.expiry) && (data.expiry * 1000 > now) &&
      !isEmpty(data.tokenType) && !isEmpty(data.uid) && !isEmpty(data.client);
  }
});
