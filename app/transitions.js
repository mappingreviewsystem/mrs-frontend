export default function(){
  // Add your transitions here, like:
  //   this.transition(
  //     this.fromRoute('people.index'),
  //     this.toRoute('people.detail'),
  //     this.use('toLeft'),
  //     this.reverse('toRight')
  //   );

  // login
  this.transition(
    this.fromRoute('login'),
    this.toRoute('planning'),
    // this.hasClass('fade-demo'),
    // this.use('fade', {duration: 3000}),
    this.use('toUp'),
    this.reverse('toDown')
  );
  this.transition(
    this.fromRoute('login'),
    this.toRoute('execution'),
    this.use('toUp'),
    this.reverse('toDown')
  );
  this.transition(
    this.fromRoute('login'),
    this.toRoute('summarization'),
    this.use('toUp'),
    this.reverse('toDown')
  );

  // steps
  this.transition(
    this.fromRoute('planning'),
    this.toRoute('execution'),
    this.use('toLeft'),
    this.reverse('toRight')
  );
  this.transition(
    this.fromRoute('planning'),
    this.toRoute('summarization'),
    this.use('toLeft'),
    this.reverse('toRight')
  );
  this.transition(
    this.fromRoute('execution'),
    this.toRoute('summarization'),
    this.use('toLeft'),
    this.reverse('toRight')
  );

  // execution
  this.transition(
    this.fromRoute('execution.databases'),
    this.toRoute('execution.selection'),
    this.use('fade'),
    this.reverse('fade')
  );
  this.transition(
    this.fromRoute('execution.databases'),
    this.toRoute('execution.extraction'),
    this.use('fade'),
    this.reverse('fade')
  );
  this.transition(
    this.fromRoute('execution.selection'),
    this.toRoute('execution.extraction'),
    this.use('fade'),
    this.reverse('fade')
  );

  // summarization
  this.transition(
    this.fromRoute('summarization.research-answers'),
    this.toRoute('summarization.statistics'),
    this.use('fade'),
    this.reverse('fade')
  );
  this.transition(
    this.fromRoute('summarization.research-answers'),
    this.toRoute('summarization.reference-graph'),
    this.use('fade'),
    this.reverse('fade')
  );
  this.transition(
    this.fromRoute('summarization.statistics'),
    this.toRoute('summarization.reference-graph'),
    this.use('fade'),
    this.reverse('fade')
  );
}
