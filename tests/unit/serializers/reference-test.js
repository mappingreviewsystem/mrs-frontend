import { moduleForModel, test } from 'ember-qunit';

moduleForModel('reference', 'Unit | Serializer | reference', {
  // Specify the other units that are required for this test.
  needs: ['serializer:reference']
});

// Replace this with your real tests.
test('it serializes records', function(assert) {
  let record = this.subject();

  let serializedRecord = record.serialize();

  assert.ok(serializedRecord);
});
