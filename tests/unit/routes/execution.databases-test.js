import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | execution.databases', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:execution.databases');
    assert.ok(route);
  });
});
