import { moduleForModel, test } from 'ember-qunit';

moduleForModel('research-question', 'Unit | Model | research question', {
  // Specify the other units that are required for this test.
  needs: ['model:research', 'model:extraction-question', 'model:article-research-question', 'model:article']
});

test('it exists', function(assert) {
  let model = this.subject();
  // let store = this.store();
  assert.ok(!!model);
});
