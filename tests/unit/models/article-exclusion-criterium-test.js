import { moduleForModel, test } from 'ember-qunit';

moduleForModel('article-exclusion-criterium', 'Unit | Model | article exclusion criterium', {
  // Specify the other units that are required for this test.
  needs: ['model:article', 'model:exclusion-criterium']
});

test('it exists', function(assert) {
  let model = this.subject();
  // let store = this.store();
  assert.ok(!!model);
});
