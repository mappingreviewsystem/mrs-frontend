import { moduleForModel, test } from 'ember-qunit';

moduleForModel('research-research-question', 'Unit | Model | research research question', {
  // Specify the other units that are required for this test.
  needs: ['model:research', 'model:research-question']
});

test('it exists', function(assert) {
  let model = this.subject();
  // let store = this.store();
  assert.ok(!!model);
});
