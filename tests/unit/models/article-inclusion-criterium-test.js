import { moduleForModel, test } from 'ember-qunit';

moduleForModel('article-inclusion-criterium', 'Unit | Model | article inclusion criterium', {
  // Specify the other units that are required for this test.
  needs: ['model:article', 'model:inclusion-criterium']
});

test('it exists', function(assert) {
  let model = this.subject();
  // let store = this.store();
  assert.ok(!!model);
});
