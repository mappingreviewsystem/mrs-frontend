import { moduleForModel, test } from 'ember-qunit';

moduleForModel('research-article-databasis', 'Unit | Model | research article databasis', {
  // Specify the other units that are required for this test.
  needs: ['model:research', 'model:article-database']
});

test('it exists', function(assert) {
  let model = this.subject();
  // let store = this.store();
  assert.ok(!!model);
});
