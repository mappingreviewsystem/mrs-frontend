import { moduleForModel, test } from 'ember-qunit';

moduleForModel('inclusion-criterium', 'Unit | Model | inclusion criterium', {
  // Specify the other units that are required for this test.
  needs: ['model:research', 'model:article']
});

test('it exists', function(assert) {
  let model = this.subject();
  // let store = this.store();
  assert.ok(!!model);
});
