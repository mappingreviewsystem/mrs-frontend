import { moduleForModel, test } from 'ember-qunit';

moduleForModel('group-membership', 'Unit | Model | group membership', {
  // Specify the other units that are required for this test.
  needs: ['model:group', 'model:user']
});

test('it exists', function(assert) {
  let model = this.subject();
  // let store = this.store();
  assert.ok(!!model);
});
