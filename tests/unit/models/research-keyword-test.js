import { moduleForModel, test } from 'ember-qunit';

moduleForModel('research-keyword', 'Unit | Model | research keyword', {
  // Specify the other units that are required for this test.
  needs: ['model:research', 'model:keyword']
});

test('it exists', function(assert) {
  let model = this.subject();
  // let store = this.store();
  assert.ok(!!model);
});
