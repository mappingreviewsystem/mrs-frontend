import { moduleForModel, test } from 'ember-qunit';

moduleForModel('article', 'Unit | Model | article', {
  // Specify the other units that are required for this test.
  needs: ['model:article-database', 'model:research', 'model:publication', 'model:publisher', 'model:note', 'model:tag', 'model:author', 'model:keyword', 'model:article-page', 'model:inclusion-criterium', 'model:exclusion-criterium', 'model:extraction-question', 'model:referenced-by', 'model:reference']
});

test('it exists', function(assert) {
  let model = this.subject();
  // let store = this.store();
  assert.ok(!!model);
});
