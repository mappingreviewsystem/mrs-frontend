import { moduleForModel, test } from 'ember-qunit';

moduleForModel('article-extraction-question', 'Unit | Model | article extraction question', {
  // Specify the other units that are required for this test.
  needs: ['model:article', 'model:extraction-question']
});

test('it exists', function(assert) {
  let model = this.subject();
  // let store = this.store();
  assert.ok(!!model);
});
