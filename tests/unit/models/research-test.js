import { moduleForModel, test } from 'ember-qunit';

moduleForModel('research', 'Unit | Model | research', {
  // Specify the other units that are required for this test.
  needs: ['model:user', 'model:group', 'model:keyword', 'model:article', 'model:article-reference', 'model:tag', 'model:article-databasis', 'model:research-question', 'model:extraction-question', 'model:inclusion-criterium', 'model:exclusion-criterium']
});

test('it exists', function(assert) {
  let model = this.subject();
  // let store = this.store();
  assert.ok(!!model);
});
