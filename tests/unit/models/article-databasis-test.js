import { moduleForModel, test } from 'ember-qunit';

moduleForModel('article-databasis', 'Unit | Model | article databasis', {
  // Specify the other units that are required for this test.
  needs: ['model:research-article-databasis', 'model:research', 'model:article']
});

test('it exists', function(assert) {
  let model = this.subject();
  // let store = this.store();
  assert.ok(!!model);
});
