import { moduleForModel, test } from 'ember-qunit';

moduleForModel('extraction-question', 'Unit | Model | extraction question', {
  // Specify the other units that are required for this test.
  needs: ['model:research', 'model:research-question', 'model:article']
});

test('it exists', function(assert) {
  let model = this.subject();
  // let store = this.store();
  assert.ok(!!model);
});
