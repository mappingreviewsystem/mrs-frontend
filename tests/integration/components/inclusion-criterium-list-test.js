import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('inclusion-criterium-list', 'Integration | Component | inclusion criterium list', {
  integration: true
});

test('it renders', function(assert) {
  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{inclusion-criterium-list}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:
  this.render(hbs`
    {{#inclusion-criterium-list}}
      template block text
    {{/inclusion-criterium-list}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
